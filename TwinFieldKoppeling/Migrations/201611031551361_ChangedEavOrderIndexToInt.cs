namespace TwinFieldKoppeling.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedEavOrderIndexToInt : DbMigration
    {
        public override void Up()
        {
            //AlterColumn("dbo.eavAttributes", "orderIndex", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.eavAttributes", "orderIndex", c => c.Short(nullable: false));
        }
    }
}
