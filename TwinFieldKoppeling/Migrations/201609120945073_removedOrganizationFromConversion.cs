namespace TwinFieldKoppeling.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class removedOrganizationFromConversion : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.orders", "conversionId", "dbo.conversions");
            DropForeignKey("dbo.orderDetails", "conversionId", "dbo.orders");
            DropForeignKey("dbo.orderOptions", "conversionId", "dbo.orders");
            DropIndex("dbo.orders", new[] { "conversionId" });
            RenameColumn(table: "dbo.conversions", name: "organizationId", newName: "Organization_organizationId");
            RenameColumn(table: "dbo.orders", name: "Organization_organizationId", newName: "organizationId");
            RenameIndex(table: "dbo.conversions", name: "IX_organizationId", newName: "IX_Organization_organizationId");
            RenameIndex(table: "dbo.orders", name: "IX_Organization_organizationId", newName: "IX_organizationId");
            DropPrimaryKey("dbo.orders");
            AddColumn("dbo.orders", "orderId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.orders", "orderId");
            AddForeignKey("dbo.orderDetails", "conversionId", "dbo.orders", "orderId");
            AddForeignKey("dbo.orderOptions", "conversionId", "dbo.orders", "orderId");
            DropColumn("dbo.orders", "conversionId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.orders", "conversionId", c => c.Int(nullable: false));
            DropForeignKey("dbo.orderOptions", "conversionId", "dbo.orders");
            DropForeignKey("dbo.orderDetails", "conversionId", "dbo.orders");
            DropPrimaryKey("dbo.orders");
            DropColumn("dbo.orders", "orderId");
            AddPrimaryKey("dbo.orders", "conversionId");
            RenameIndex(table: "dbo.orders", name: "IX_organizationId", newName: "IX_Organization_organizationId");
            RenameIndex(table: "dbo.conversions", name: "IX_Organization_organizationId", newName: "IX_organizationId");
            RenameColumn(table: "dbo.orders", name: "organizationId", newName: "Organization_organizationId");
            RenameColumn(table: "dbo.conversions", name: "Organization_organizationId", newName: "organizationId");
            CreateIndex("dbo.orders", "conversionId");
            AddForeignKey("dbo.orderOptions", "conversionId", "dbo.orders", "conversionId");
            AddForeignKey("dbo.orderDetails", "conversionId", "dbo.orders", "conversionId");
            AddForeignKey("dbo.orders", "conversionId", "dbo.conversions", "conversionId");
        }
    }
}
