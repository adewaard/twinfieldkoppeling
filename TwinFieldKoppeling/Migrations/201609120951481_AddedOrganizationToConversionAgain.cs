namespace TwinFieldKoppeling.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedOrganizationToConversionAgain : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.conversions", "organizationId", c => c.Int(nullable: true));
            CreateIndex("dbo.conversions", "organizationId");
            AddForeignKey("dbo.conversions", "organizationId", "dbo.organizations", "organizationId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.conversions", name: "IX_organizationId", newName: "IX_Organization_organizationId");
            RenameColumn(table: "dbo.conversions", name: "organizationId", newName: "Organization_organizationId");
        }
    }
}
