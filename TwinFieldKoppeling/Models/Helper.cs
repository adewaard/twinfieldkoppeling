﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TwinFieldKoppeling.DataAccess;

namespace TwinFieldKoppeling.Models
{
    public class Helper
    {
        public void deleteLogs()
        {
            using (DbContext context = new DbContext())
            {
                DateTime oldDate = DateTime.Now.AddDays(-30);
                List<Log> logs = context.logging.Where(l => l.date < oldDate).ToList();

                if (logs.Count > 0)
                {
                    context.logging.RemoveRange(logs);
                    context.SaveChanges();
                }
            }
        }
    }
}
