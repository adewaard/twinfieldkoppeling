﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using log4net;
using TwinFieldKoppeling.DataAccess.Models;
using TwinFieldKoppeling.DataAccess.Models.Catalog;
using TwinFieldKoppeling.DataAccess.Models.Conversion;
using TwinFieldKoppeling.DataAccess.Models.EAV;
using TwinFieldKoppeling.DataAccess.Models.Webshop;
using DbContext = TwinFieldKoppeling.DataAccess.DbContext;

namespace TwinFieldKoppeling.Models
{
    public class Invoice
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Invoice));
        private static int conversionId = 0;
        private static string cluster;

        public void getConversionsWithStatusFinal(string sessionId, string clusterArg)
        {
            Logger.Info("Started updating invoices");
            cluster = clusterArg;

            //Get organisation
            using (DbContext context = new DbContext())
            {
                int organizationId = 0;
                bool updateInvoiceId = false;
                string tempInvoiceId = "";
                bool useTempInvoiceId = false;
                string organizationUpdateType = "";
                //string workflowNameParameter = ConfigurationManager.AppSettings["twinfieldWorkflowName"].Trim();

                var sql = @"SELECT c.*
                            FROM conversions c
                            LEFT JOIN workflows w on c.conversionId = w.conversionId
                            WHERE c.conversionStatus = @conversionStatus
                            AND w.completed = 1
                            ORDER BY organizationId;";
                List<SqlParameter> parameterList = new List<SqlParameter> { new SqlParameter("@conversionStatus", (int)ConversionStatus.Completed) };
                SqlParameter[] parameters = parameterList.ToArray();
                var conversions =
                context.Database.SqlQuery<Conversion>(sql, parameters).ToList();
                //conversions = conversions.Where(x => x.workflows.FirstOrDefault().completed).ToList();
                //var conversions = context.conversions.Where(o => o.conversionStatus == (int)ConversionStatus.Completed && o.workflows.FirstOrDefault().completed).OrderBy(o => o.organizationId).ToList();

                if (conversions.Count == 0)
                {
                    Logger.Info("No new conversions to update");
                    return;
                }

                foreach (Conversion conversion in conversions)
                {
                    try
                    {
                        conversionId = conversion.conversionId;
                        var eavAssociations =
                            context.organizations.Find(conversion.organizationId).eavAssociations.Where(
                                e =>
                                    e.eavAttributeValue.eavAttribute.name == "organizationCustomerId" ||
                                    e.eavAttributeValue.eavAttribute.name == "organizationInvoiceId").ToList();

                        string organizationCustomerId =
                            eavAssociations
                                .FirstOrDefault(e => e.eavAttributeValue.eavAttribute.name == "organizationCustomerId")?
                                .eavAttributeValue.value;

                        string organizationInvoiceId =
                            eavAssociations
                                .FirstOrDefault(e => e.eavAttributeValue.eavAttribute.name == "organizationInvoiceId")?
                                .eavAttributeValue.value;

                        if (string.IsNullOrEmpty(organizationCustomerId))
                        {
                            Console.WriteLine("No customerId for this organization");
                            continue;
                        }

                        if (string.IsNullOrEmpty(organizationInvoiceId) && string.IsNullOrEmpty(tempInvoiceId))
                        {
                            updateInvoiceId = true;
                        }


                        if (string.IsNullOrEmpty(organizationInvoiceId) && useTempInvoiceId)
                        {
                            organizationInvoiceId = tempInvoiceId;
                        }

                        var priceToUse = (conversion.conversionTotalPrice / 10).ToString();
                        string invoiceId = getInvoiceXml(sessionId, priceToUse
                            , organizationCustomerId,
                            organizationInvoiceId);

                        Console.WriteLine("Returned " + invoiceId);

                        if (!string.IsNullOrEmpty(organizationInvoiceId) && invoiceId != organizationInvoiceId &&
                            invoiceId != "update" && invoiceId != "nothing")
                        {
                            updateInvoiceId = true;
                            organizationUpdateType = "update";
                        }

                        if (organizationId == 0 || organizationId != conversion.organizationId.Value)
                        {
                            organizationId = conversion.organizationId.Value;
                            useTempInvoiceId = false;
                            tempInvoiceId = "";
                        }
                        else if (organizationId == conversion.organizationId.Value ||
                                 (organizationInvoiceId != invoiceId && !string.IsNullOrEmpty(organizationInvoiceId)))
                        {
                            tempInvoiceId = invoiceId;
                            useTempInvoiceId = true;
                            organizationId = conversion.organizationId.Value;
                        }

                        if (updateInvoiceId)
                        {
                            updateOrganization(conversion.organizationId.Value, invoiceId, organizationUpdateType);
                            updateInvoiceId = false;
                        }

                        //Update conversion status
                        conversion.conversionStatus = 7000;
                        context.Entry(conversion).State = EntityState.Modified;
                        context.conversions.AddOrUpdate(conversion);
                        context.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        Logger.Error("Error Message: " + ex.Message.ToString(), ex);
                    }
                }

                Logger.Info("Updated " + conversions.Count + " conversions");
            }
           
        }

        private void updateOrganization(int organizationId, string invoiceId, string organizationUpdateType)
        {
            using (DbContext context = new DbContext())
            {
                Organization organizationToUpdate = context.organizations.Find(organizationId);

                if (organizationUpdateType == "update")
                {
                    organizationToUpdate.eavAssociations.FirstOrDefault(
                        e => e.eavAttributeValue.eavAttribute.name == "organizationInvoiceId").eavAttributeValue.value =
                        invoiceId;
                }
                else
                {
                    organizationToUpdate.eavAssociations.Add(new EavAssociation
                    {
                        eavAttributeValue = new EavAttributeValue
                        {
                            eavAttribute = context.eavAttributes.FirstOrDefault(e => e.name == "organizationInvoiceId"),
                            value = invoiceId
                        }
                    });
                    
                }
                context.organizations.AddOrUpdate(organizationToUpdate);
                context.SaveChanges();
            }
        }

        private string getInvoiceXml(string sessionId, string totalPrice, string organizationCustomerId, string organizationInvoiceId)
        {
            if (string.IsNullOrEmpty(organizationInvoiceId))
            {
                string invoiceId = createNewInvoice(sessionId, organizationCustomerId, totalPrice);
                return invoiceId;
            }

            // Create instance of process xml web service.
            TwinfieldProcessXml.ProcessXml processXml = new TwinfieldProcessXml.ProcessXml();
            processXml.Url = cluster + "/webservices/processxml.asmx";

            // Create and assign the header.
            processXml.HeaderValue = new TwinfieldProcessXml.Header();
            processXml.HeaderValue.SessionID = sessionId;
            
            // Create the request document.
            XmlDocument doc = new XmlDocument();
            //(1) the xml declaration is recommended, but not mandatory
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            //Fill the request Document

            //(2) string.Empty makes cleaner code
            XmlElement element1 = doc.CreateElement(string.Empty, "read", string.Empty);
            doc.AppendChild(element1);

            XmlElement element2 = doc.CreateElement(string.Empty, "type", string.Empty);
            XmlText text = doc.CreateTextNode("salesinvoice");
            element2.AppendChild(text);
            element1.AppendChild(element2);

            XmlElement officeElement = doc.CreateElement(string.Empty, "office", string.Empty);
            XmlText officeText = doc.CreateTextNode(ConfigurationManager.AppSettings["twinfieldOffice"].Trim());
            officeElement.AppendChild(officeText);
            element1.AppendChild(officeElement);

            XmlElement element3 = doc.CreateElement(string.Empty, "code", string.Empty);
            XmlText text1 = doc.CreateTextNode("FACTUUR");
            element3.AppendChild(text1);
            element1.AppendChild(element3);

            XmlElement element4 = doc.CreateElement(string.Empty, "invoicenumber", string.Empty);
            XmlText text2 = doc.CreateTextNode(organizationInvoiceId);
            element4.AppendChild(text2);
            element1.AppendChild(element4);

            // Start processing the xml.
            XmlNode xmlDoc = processXml.ProcessXmlDocument(doc);

            if (xmlDoc.SelectSingleNode("//header/@error") != null)
            {
                Console.WriteLine("ERROR: " + xmlDoc.SelectSingleNode("//header/@msg").InnerXml);
            }
            else
            {
                try
                {
                    string invoiceStatus = xmlDoc.SelectSingleNode("//header/status").InnerXml;
                    Console.WriteLine("Retrieved Invoice Status: " + xmlDoc.SelectSingleNode("//header/status").InnerXml);

                    //If Status is concept we can add lines. If not, we create a new invoice.
                    if (invoiceStatus == "concept")
                    {
                        var lines = xmlDoc.SelectNodes("//lines/line");
                        addInvoiceLines(sessionId, lines, organizationCustomerId, organizationInvoiceId, totalPrice);
                        return "update";
                    }
                    else if (invoiceStatus == "final")
                    {
                        string invoiceId = createNewInvoice(sessionId, organizationCustomerId, totalPrice);
                        return invoiceId;
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Error Message: " + ex.Message.ToString(), ex);
                }
            }

            return "nothing";
        }

        private static string createNewInvoice(string sessionId, string organizationCustomerId, string totalPrice)
        {
            var conversion = new Conversion();
            var organisation = new Organization();
            using (DbContext db = new DbContext())
            {
                conversion = db.conversions.Find(conversionId);
                organisation = db.organizations.Find(conversion.organizationId);
            }

            // Create instance of process xml web service.
            TwinfieldProcessXml.ProcessXml processXml = new TwinfieldProcessXml.ProcessXml();
            processXml.Url = cluster + "/webservices/processxml.asmx";

            // Create and assign the header.
            processXml.HeaderValue = new TwinfieldProcessXml.Header();
            processXml.HeaderValue.SessionID = sessionId;

            // Create the request document.
            XmlDocument doc = new XmlDocument();
            //(1) the xml declaration is recommended, but not mandatory
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            //Fill the request Document
            XmlElement transactionElement = doc.CreateElement(string.Empty, "salesinvoice", string.Empty);
            doc.AppendChild(transactionElement);

            XmlElement headerElement = doc.CreateElement(string.Empty, "header", string.Empty);
            transactionElement.AppendChild(headerElement);

            XmlElement officeElement = doc.CreateElement(string.Empty, "office", string.Empty);
            XmlText officeText = doc.CreateTextNode(ConfigurationManager.AppSettings["twinfieldOffice"].Trim());
            officeElement.AppendChild(officeText);
            headerElement.AppendChild(officeElement);

            XmlElement invoicetypeElement = doc.CreateElement(string.Empty, "invoicetype", string.Empty);
            XmlText invoicetypeText = doc.CreateTextNode("FACTUUR");
            invoicetypeElement.AppendChild(invoicetypeText);
            headerElement.AppendChild(invoicetypeElement);

            XmlElement customerElement = doc.CreateElement(string.Empty, "customer", string.Empty);
            XmlText customerText = doc.CreateTextNode(organizationCustomerId);
            customerElement.AppendChild(customerText);
            headerElement.AppendChild(customerElement);

            XmlElement statusElement = doc.CreateElement(string.Empty, "status", string.Empty);
            XmlText statusText = doc.CreateTextNode("concept");
            statusElement.AppendChild(statusText);
            headerElement.AppendChild(statusElement);

            XmlElement currencyElement = doc.CreateElement(string.Empty, "currency", string.Empty);
            XmlText currencyText = doc.CreateTextNode("EUR");
            currencyElement.AppendChild(currencyText);
            headerElement.AppendChild(currencyElement);

            XmlElement periodElement = doc.CreateElement(string.Empty, "period", string.Empty);
            XmlText periodText = doc.CreateTextNode(DateTime.Now.Year + "/" + DateTime.Now.Month);
            periodElement.AppendChild(periodText);
            headerElement.AppendChild(periodElement);

            XmlElement paymentmethodElement = doc.CreateElement(string.Empty, "paymentmethod", string.Empty);
            XmlText paymentmethodText = doc.CreateTextNode("bank");
            paymentmethodElement.AppendChild(paymentmethodText);
            headerElement.AppendChild(paymentmethodElement);

            XmlElement linesElement = doc.CreateElement(string.Empty, "lines", string.Empty);
            transactionElement.AppendChild(linesElement);

            XmlElement lineElement = doc.CreateElement(string.Empty, "line", string.Empty);
            //lineElement.SetAttribute("id", "2");

            XmlElement articleElement = doc.CreateElement(string.Empty, "article", string.Empty);
            XmlText articleText = doc.CreateTextNode(ConfigurationManager.AppSettings["invoiceProductId"].Trim());
            articleElement.AppendChild(articleText);
            lineElement.AppendChild(articleElement);

            XmlElement subArticleElement = doc.CreateElement(string.Empty, "subarticle", string.Empty);
            XmlText subArticleText = doc.CreateTextNode(ConfigurationManager.AppSettings["invoiceSubProductId"].Trim());
            subArticleElement.AppendChild(subArticleText);
            lineElement.AppendChild(subArticleElement);

            string description = "Hapjes aan Huis Provisie - " + conversion.conversionId + " - " + organisation.name ;
            if (!string.IsNullOrEmpty(conversion.conversionReference))
            {
                description += " - Referentie: " + conversion.conversionReference;
            }

            XmlElement descriptionElement = doc.CreateElement(string.Empty, "description", string.Empty);
            XmlText escriptionText = doc.CreateTextNode(description);
            descriptionElement.AppendChild(escriptionText);
            lineElement.AppendChild(descriptionElement);

            XmlElement quantityElement = doc.CreateElement(string.Empty, "quantity", string.Empty);
            XmlText quantityText = doc.CreateTextNode("1.00");
            quantityElement.AppendChild(quantityText);
            lineElement.AppendChild(quantityElement);
            
            XmlElement vatCodeElement = doc.CreateElement(string.Empty, "vatcode", string.Empty);
            XmlText vatCodeText = doc.CreateTextNode("VH");
            vatCodeElement.AppendChild(vatCodeText);
            lineElement.AppendChild(vatCodeElement);

            XmlElement unitsElement = doc.CreateElement(string.Empty, "units", string.Empty);
            XmlText unitsText = doc.CreateTextNode("1");
            unitsElement.AppendChild(unitsText);
            lineElement.AppendChild(unitsElement);

            XmlElement unitspriceexclElement = doc.CreateElement(string.Empty, "unitspriceexcl", string.Empty);
            XmlText unitspriceexclText = doc.CreateTextNode(totalPrice);
            unitspriceexclElement.AppendChild(unitspriceexclText);
            lineElement.AppendChild(unitspriceexclElement);

            linesElement.AppendChild(lineElement);

            // Start processing the xml.
            XmlNode xmlDoc = processXml.ProcessXmlDocument(doc);

            if (xmlDoc.SelectSingleNode("//header/@error") != null)
            {
                Console.WriteLine("ERROR: " + xmlDoc.SelectSingleNode("//header/@msg").InnerXml);
                return "nothing";
            }
            else
            {
                Console.WriteLine("Status new invoice " + xmlDoc.SelectSingleNode("//header/invoicenumber").InnerXml + ": " + xmlDoc.SelectSingleNode("//header/status").InnerXml);
                return xmlDoc.SelectSingleNode("//header/invoicenumber").InnerXml;
            }
        }

        private static void addInvoiceLines(string sessionId, XmlNodeList lines, string organizationCustomerId, string organizationInvoiceId, string totalPrice)
        {
            // Create instance of process xml web service.
            TwinfieldProcessXml.ProcessXml processXml = new TwinfieldProcessXml.ProcessXml();
            processXml.Url = cluster + "/webservices/processxml.asmx";

            // Create and assign the header.
            processXml.HeaderValue = new TwinfieldProcessXml.Header();
            processXml.HeaderValue.SessionID = sessionId;

            // Create the request document.
            XmlDocument doc = new XmlDocument();
            //(1) the xml declaration is recommended, but not mandatory
            XmlDeclaration xmlDeclaration = doc.CreateXmlDeclaration("1.0", "UTF-8", null);
            XmlElement root = doc.DocumentElement;
            doc.InsertBefore(xmlDeclaration, root);

            //Fill the request Document
            XmlElement transactionElement = doc.CreateElement(string.Empty, "salesinvoice", string.Empty);
            doc.AppendChild(transactionElement);

            XmlElement headerElement = doc.CreateElement(string.Empty, "header", string.Empty);
            transactionElement.AppendChild(headerElement);

            XmlElement officeElement = doc.CreateElement(string.Empty, "office", string.Empty);
            XmlText officeText = doc.CreateTextNode(ConfigurationManager.AppSettings["twinfieldOffice"].Trim());
            officeElement.AppendChild(officeText);
            headerElement.AppendChild(officeElement);

            XmlElement invoicetypeElement = doc.CreateElement(string.Empty, "invoicetype", string.Empty);
            XmlText invoicetypeText = doc.CreateTextNode("FACTUUR");
            invoicetypeElement.AppendChild(invoicetypeText);
            headerElement.AppendChild(invoicetypeElement);

            XmlElement bankElement = doc.CreateElement(string.Empty, "invoicenumber", string.Empty);
            XmlText bankText = doc.CreateTextNode(organizationInvoiceId);
            bankElement.AppendChild(bankText);
            headerElement.AppendChild(bankElement);

            XmlElement customerElement = doc.CreateElement(string.Empty, "customer", string.Empty);
            XmlText customerText = doc.CreateTextNode(organizationCustomerId);
            customerElement.AppendChild(customerText);
            headerElement.AppendChild(customerElement);

            XmlElement statusElement = doc.CreateElement(string.Empty, "status", string.Empty);
            XmlText statusText = doc.CreateTextNode("concept");
            statusElement.AppendChild(statusText);
            headerElement.AppendChild(statusElement);

            XmlElement currencyElement = doc.CreateElement(string.Empty, "currency", string.Empty);
            XmlText currencyText = doc.CreateTextNode("EUR");
            currencyElement.AppendChild(currencyText);
            headerElement.AppendChild(currencyElement);

            XmlElement periodElement = doc.CreateElement(string.Empty, "period", string.Empty);
            XmlText periodText = doc.CreateTextNode(DateTime.Now.Year + "/" + DateTime.Now.Month);
            periodElement.AppendChild(periodText);
            headerElement.AppendChild(periodElement);

            XmlElement paymentmethodElement = doc.CreateElement(string.Empty, "paymentmethod", string.Empty);
            XmlText paymentmethodText = doc.CreateTextNode("bank");
            paymentmethodElement.AppendChild(paymentmethodText);
            headerElement.AppendChild(paymentmethodElement);

            XmlElement linesElement = doc.CreateElement(string.Empty, "lines", string.Empty);
            transactionElement.AppendChild(linesElement);

            foreach (XmlNode line in lines)
            {
                var importLine = doc.ImportNode(line, true);
                linesElement.AppendChild(importLine);
            }

            //Create new element based on order values
            XmlElement lineElement = doc.CreateElement(string.Empty, "line", string.Empty);
            //lineElement.SetAttribute("id", "2");

            XmlElement articleElement = doc.CreateElement(string.Empty, "article", string.Empty);
            XmlText articleText = doc.CreateTextNode("0");
            articleElement.AppendChild(articleText);
            lineElement.AppendChild(articleElement);

            XmlElement subArticleElement = doc.CreateElement(string.Empty, "subarticle", string.Empty);
            XmlText subArticleText = doc.CreateTextNode("-");
            subArticleElement.AppendChild(subArticleText);
            lineElement.AppendChild(subArticleElement);

            XmlElement quantityElement = doc.CreateElement(string.Empty, "quantity", string.Empty);
            XmlText quantityText = doc.CreateTextNode("1.00");
            quantityElement.AppendChild(quantityText);
            lineElement.AppendChild(quantityElement);

            XmlElement unitsElement = doc.CreateElement(string.Empty, "units", string.Empty);
            XmlText unitsText = doc.CreateTextNode("1");
            unitsElement.AppendChild(unitsText);
            lineElement.AppendChild(unitsElement);

            XmlElement unitspriceexclElement = doc.CreateElement(string.Empty, "unitspriceexcl", string.Empty);
            XmlText unitspriceexclText = doc.CreateTextNode(totalPrice);
            unitspriceexclElement.AppendChild(unitspriceexclText);
            lineElement.AppendChild(unitspriceexclElement);

            linesElement.AppendChild(lineElement);

            
            // Start processing the xml.
            XmlNode xmlDoc = processXml.ProcessXmlDocument(doc);

            if (xmlDoc.SelectSingleNode("//header/@error") != null)
            {
                Console.WriteLine("ERROR: " + xmlDoc.SelectSingleNode("//header/@msg").InnerXml);
            }
            else
            {
                Console.WriteLine("Updated invoice " + xmlDoc.SelectSingleNode("//header/invoicenumber").InnerXml);
            }
        }
    }
}
