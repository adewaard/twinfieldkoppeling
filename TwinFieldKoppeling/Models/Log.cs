﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinFieldKoppeling.Models
{
    public class Log
    {
        [Key]
        public int Id { get; set; }
        [Required]
        public DateTime date { get; set; }
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string Thread { get; set; }
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string Level { get; set; }
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string Logger { get; set; }
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(4000)]
        public string Message { get; set; }
        [Column(TypeName = "nvarchar")]
        [MaxLength(2000)]
        public string Exception { get; set; }
    }
}
