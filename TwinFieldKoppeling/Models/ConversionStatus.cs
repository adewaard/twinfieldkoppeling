﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TwinFieldKoppeling.Models
{
    public enum ConversionStatus
    {
        Processing = 0000,
        Completed = 5000,
        Invoiced = 7000,
        Canceled = 9000,
        Error = 9999
    }
}
