﻿using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Diagnostics;
using System.Linq;
using TwinFieldKoppeling.DataAccess.Models;
using TwinFieldKoppeling.DataAccess.Models.Catalog;
using TwinFieldKoppeling.DataAccess.Models.Conversion;
using TwinFieldKoppeling.DataAccess.Models.EAV;
using TwinFieldKoppeling.DataAccess.Models.Webshop;
using TwinFieldKoppeling.Models;
using DateTime = System.DateTime;

namespace TwinFieldKoppeling.DataAccess
{
    class DbContext : System.Data.Entity.DbContext
    {
        public DbSet<AlternateContent> alternateContent { get; set; }
        public DbSet<Address> addresses { get; set; }
        public DbSet<AwsSetting> awsSettings { get; set; }
        public DbSet<Classification> classifications { get; set; }
        public DbSet<Country> countries { get; set; }
        public DbSet<Css> css { get; set; }
        public DbSet<DocumentItem> documentItems { get; set; }
        public DbSet<Document> documents { get; set; }
        public DbSet<File> files { get; set; }
        public DbSet<FileImage> fileImages { get; set; }
        public DbSet<Folder> folders { get; set; }
        public DbSet<GlobalSetting> globalSettings { get; set; }
        public DbSet<Html> html { get; set; }
        public DbSet<Javascript> javascript { get; set; }
        public DbSet<Language> languages { get; set; }
        public DbSet<MediaType> mediaTypes { get; set; }
        public DbSet<MetaItem> metaItems { get; set; }
        public DbSet<Uri> uris { get; set; }
        public DbSet<Module> modules { get; set; }
        public DbSet<Person> persons { get; set; }
        public DbSet<PublishedItem> publishedItems { get; set; }
        public DbSet<Role> customRoles { get; set; }
        public DbSet<RoleAccess> roleAccess { get; set; }
        public DbSet<ScheduledTask> scheduledTasks { get; set; }
        public DbSet<SeoLinkWord> seoLinkWords { get; set; }

        public DbSet<Site> sites { get; set; }
        public DbSet<SiteLanguage> multipleSiteLanguages { get; set; }
        public DbSet<SiteProperties> siteProperties { get; set; }
        public DbSet<Taxonomy> taxonomies { get; set; }
        public DbSet<Template> templates { get; set; }
        public DbSet<TouchIcon> touchIcons { get; set; }
        public DbSet test { get; set; }

        public DbSet<ContactPerson> contactPersons { get; set; }
        public DbSet<Organization> organizations { get; set; }

        //Catalog
        public DbSet<StoreView> storeViews { get; set; }
        public DbSet<Product> products { get; set; }
        public DbSet<Category> categories { get; set; }
        public DbSet<VatRate> vatRates { get; set; }
        public DbSet<CategoryImage> categoryImages { get; set; }
        public DbSet<CategoryImageFile> categoryImageFiles { get; set; }
        public DbSet<ConfigurableProduct> configurableProducts { get; set; }
        public DbSet<FeaturedProduct> featuredProducts { get; set; }
        public DbSet<AssociatedProduct> associatedProducts { get; set; }
        public DbSet<ProductImage> productImages { get; set; }
        public DbSet<ProductImageFile> productImageFiles { get; set; }
        public DbSet<ImageType> imageTypes { get; set; }

        //Webshop
        public DbSet<OrderAddress> orderAddresses { get; set; }
        public DbSet<Order> orders { get; set; }
        public DbSet<OrderDetail> orderDetails { get; set; }
        public DbSet<OrderOption> orderOptions { get; set; }
        public DbSet<Shipment> shipments { get; set; }
        public DbSet<ShipmentDetail> shipmentDetails { get; set; }
        public DbSet<WebshopSetting> webshopSettings { get; set; }

        //EAV
        public DbSet<EavSet> eavSets { get; set; }
        public DbSet<EavGroup> eavGroups { get; set; }
        public DbSet<EavAttribute> eavAttributes { get; set; }
        public DbSet<EavAttributeValue> eavAttributeValues { get; set; }
        public DbSet<EavFileAttribute> eavFileAttributes { get; set; }
        public DbSet<EavFileAttributeValue> eavFileAttributeValues { get; set; }
        public DbSet<EavBooleanAttribute> eavBooleanAttributes { get; set; }
        public DbSet<EavBooleanAttributeValue> eavBooleanAttributeValues { get; set; }
        public DbSet<EavLocationAttribute> eavLocationAttributes { get; set; }
        public DbSet<EavLocationAttributeValue> eavLocationAttributeValues { get; set; }
        public DbSet<EavHtmlAttribute> eavHtmlAttributes { get; set; }
        public DbSet<EavHtmlAttributeValue> eavHtmlAttributeValues { get; set; }
        public DbSet<EavCollectionAttribute> eavCollectionAttributes { get; set; }
        public DbSet<EavCollectionAttributeValue> eavCollectionAttributeValues { get; set; }
        public DbSet<EavCollectionAttributeItem> eavCollectionAttributeItems { get; set; }
        public DbSet<EavLabel> eavLabels { get; set; }
        public DbSet<EavAssociation> eavAssociations { get; set; }

        //Form
        public DbSet<Conversion> conversions { get; set; }

        public DbSet<Log> logging { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Address>().ToTable("addresses");
            modelBuilder.Entity<AlternateContent>().ToTable("alternateContent");
            modelBuilder.Entity<AwsSetting>().ToTable("awsSettings");
            modelBuilder.Entity<Classification>().ToTable("classifications");
            modelBuilder.Entity<Country>().ToTable("countries");
            modelBuilder.Entity<Css>().ToTable("css");
            modelBuilder.Entity<DocumentItem>().ToTable("documentItems");
            modelBuilder.Entity<Document>().ToTable("documents");
            modelBuilder.Entity<File>().ToTable("files");
            modelBuilder.Entity<FileImage>().ToTable("fileImages");
            modelBuilder.Entity<Folder>().ToTable("folders");
            modelBuilder.Entity<GlobalSetting>().ToTable("globalSettings");
            modelBuilder.Entity<Html>().ToTable("html");
            modelBuilder.Entity<Javascript>().ToTable("javascript");
            modelBuilder.Entity<Language>().ToTable("languages");
            modelBuilder.Entity<MediaType>().ToTable("mediaTypes");
            modelBuilder.Entity<MetaItem>().ToTable("metaItems");
            modelBuilder.Entity<Uri>().ToTable("uris");
            modelBuilder.Entity<Module>().ToTable("modules");
            modelBuilder.Entity<Person>().ToTable("persons");
            modelBuilder.Entity<PublishedItem>().ToTable("publishedItems");
            modelBuilder.Entity<Role>().ToTable("roleAccess");
            modelBuilder.Entity<ScheduledTask>().ToTable("scheduledTasks");
            modelBuilder.Entity<SeoLinkWord>().ToTable("seoLinkWords");
            modelBuilder.Entity<Site>().ToTable("sites");

            modelBuilder.Entity<Site>().
                HasMany(c => c.multipleSiteLanguages).
                WithMany(p => p.multipleSitesWithLanguages).
                Map(
                    m =>
                    {
                        m.MapLeftKey("siteId");
                        m.MapRightKey("languageId");
                        m.ToTable("siteLanguages");
                    });

            modelBuilder.Entity<SiteLanguage>().ToTable("multipleSiteLanguages");
            modelBuilder.Entity<SiteProperties>().ToTable("siteProperties");

            modelBuilder.Entity<Taxonomy>().ToTable("taxonomies");
            modelBuilder.Entity<Template>().ToTable("templates");
            modelBuilder.Entity<TouchIcon>().ToTable("touchIcons");

            modelBuilder.Entity<ContactPerson>().ToTable("contactPersons");
            modelBuilder.Entity<Organization>().ToTable("organizations");

            modelBuilder.Entity<StoreView>().ToTable("storeViews");
            modelBuilder.Entity<Category>().ToTable("categories");
            modelBuilder.Entity<Product>().ToTable("products");
            modelBuilder.Entity<Product>().
                HasMany(c => c.categories).
                WithMany(p => p.products).
                Map(
                    m =>
                    {
                        m.MapLeftKey("productId");
                        m.MapRightKey("categoryId");
                        m.ToTable("productCategories");
                    });
            modelBuilder.Entity<AssociatedProduct>().ToTable("associatedProducts");
            modelBuilder.Entity<ConfigurableProduct>().ToTable("configurableProducts");
            modelBuilder.Entity<FeaturedProduct>().ToTable("featuredProducts");
            modelBuilder.Entity<CategoryImage>().ToTable("categoryImages");
            modelBuilder.Entity<CategoryImageFile>().ToTable("categoryImageFiles");
            modelBuilder.Entity<ProductImage>().ToTable("productImages");
            modelBuilder.Entity<ProductImageFile>().ToTable("productImageFiles");
            modelBuilder.Entity<ImageType>().ToTable("imageTypes");

            modelBuilder.Entity<OrderAddress>().ToTable("orderAddresses");
            modelBuilder.Entity<VatRate>().ToTable("vatRates");
            modelBuilder.Entity<Order>().ToTable("orders");
            modelBuilder.Entity<OrderDetail>().ToTable("orderDetails");
            modelBuilder.Entity<OrderOption>().ToTable("orderOptions");
            modelBuilder.Entity<Shipment>().ToTable("shipments");
            modelBuilder.Entity<ShipmentDetail>().ToTable("shipmentDetails");
            modelBuilder.Entity<WebshopSetting>().ToTable("webshopSettings");

            //EAV
            modelBuilder.Entity<EavSet>().ToTable("eavSets");
            modelBuilder.Entity<EavGroup>().ToTable("eavGroups");
            modelBuilder.Entity<EavAttribute>().ToTable("eavAttributes");
            modelBuilder.Entity<EavAttributeValue>().ToTable("eavAttributeValues");
            modelBuilder.Entity<EavFileAttribute>().ToTable("eavFileAttributes");
            modelBuilder.Entity<EavFileAttributeValue>().ToTable("eavFileAttributeValues");
            modelBuilder.Entity<EavLocationAttribute>().ToTable("eavLocationAttributes");
            modelBuilder.Entity<EavLocationAttributeValue>().ToTable("eavLocationAttributeValues");
            modelBuilder.Entity<EavBooleanAttribute>().ToTable("eavBooleanAttributes");
            modelBuilder.Entity<EavBooleanAttributeValue>().ToTable("eavBooleanAttributeValues");
            modelBuilder.Entity<EavHtmlAttribute>().ToTable("eavHtmlAttributes");
            modelBuilder.Entity<EavHtmlAttributeValue>().ToTable("eavHtmlAttributeValues");
            modelBuilder.Entity<EavCollectionAttribute>().ToTable("eavCollectionAttributes");
            modelBuilder.Entity<EavCollectionAttributeValue>().ToTable("eavCollectionAttributeValues");
            modelBuilder.Entity<EavCollectionAttributeItem>().ToTable("eavCollectionAttributeItems");
            modelBuilder.Entity<EavLabel>().ToTable("eavLabels");
            modelBuilder.Entity<EavAssociation>().ToTable("eavAssociations");

            //Forms
            modelBuilder.Entity<Conversion>().ToTable("conversions");

            modelBuilder.Entity<Log>().ToTable("logging");
        }

        public override int SaveChanges()
        {
            var timestamp = DateTime.Now;

            foreach (var entry in this.ChangeTracker.Entries().Where(e => e.State == (EntityState)EntityState.Added))
            {
                if (entry.Property("dateInsert").CurrentValue.ToString().Length == 0 || entry.Property("dateInsert").CurrentValue != null)
                {
                    entry.Property("dateInsert").CurrentValue = timestamp;
                }
                //entry.Property("dateInsert").CurrentValue = timestamp;
                var prop = entry.Entity.GetType().GetProperty("dateLastUpdate");

                if (prop != null)
                {
                    entry.Property("dateLastUpdate").CurrentValue = timestamp;
                }

            }

            foreach (var entry in this.ChangeTracker.Entries().Where(e => e.State == (EntityState)EntityState.Modified))
            {
                var prop = entry.Entity.GetType().GetProperty("dateLastUpdate");

                if (prop != null)
                {
                    entry.Property("dateLastUpdate").CurrentValue = timestamp;
                }
            }
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException dbEx)
            {
                foreach (var validationErrors in dbEx.EntityValidationErrors)
                {
                    foreach (var validationError in validationErrors.ValidationErrors)
                    {
                        Debug.WriteLine(
                            "Class: {0}, Property: {1}, Error: {2}",
                            validationErrors.Entry.Entity.GetType().FullName,
                            validationError.PropertyName,
                            validationError.ErrorMessage);
                    }
                }
                throw;
            }
            //return base.SaveChanges();
        }
    }
}
