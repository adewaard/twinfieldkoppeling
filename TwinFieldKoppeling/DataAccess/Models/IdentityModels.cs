﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser 
    {
        [Key]
        [Column(TypeName = "nvarchar")]
        [MaxLength(128)]
        public string Id { get; set; }

        [Column(TypeName = "nvarchar")]
        public string applicationLanguage { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int userId { get; set; }

        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //public int? personId { get; set; }
        //[ForeignKey("personId")]
        //public virtual Person person { get; set; }

        public virtual ICollection<Person> persons { get; set; } 

        public virtual ICollection<Role> customRoles { get; set; }

        public virtual ICollection<Site> sites { get; set; }

        public virtual ICollection<Language> languages { get; set; }

        public virtual ICollection<ContactPerson> contactPersons { get; set; } 
    }


}