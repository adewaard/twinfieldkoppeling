﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Role
    {
        [Key]
        public int roleId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        public string accessifyWcmsRoleId { get; set; }
        //[ForeignKey("accessifyWcmsRoleId")]
        //public virtual ApplicationRole accessifyWcmsRole { get; set; }

        public virtual ICollection<ApplicationUser> users { get; set; }
       

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }
        
    }
}
