﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Css
    {
        //Identifier
        [Key]
        public int cssId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string name { get; set; }

        [Required]
        [Column(TypeName = "ntext")]
        public string css { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string order { get; set; }

        [Required]
        public bool active { get; set; }
        
        //Dates
        [Required]
        public DateTime dateInsert { get; set; }

        [Required]
        public DateTime dateLastUpdate { get; set; }

        //Relation from child to parent
        public int? siteId { get; set; }
        [ForeignKey("siteId")]
        public virtual Site site { get; set; }

        public int? moduleId { get; set; }
        [ForeignKey("moduleId")]
        public virtual Module module { get; set; }

        public int? mediaTypeId { get; set; }
        [ForeignKey("mediaTypeId")]
        public virtual MediaType mediatype { get; set; }
    }
}
