﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class TouchIcon
    {
        //Identifier
        [Key]
        public int touchIconId { get; set; }

        //Properties
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string rel { get; set; }

        public int width { get; set; }

        public int height { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //Relation from parent to chuild
        public int? touchIconImageId { get; set; }
        [ForeignKey("touchIconImageId")]
        public virtual FileImage fileImage { get; set; }

        public int? sitePropertiesId { get; set; }
        [ForeignKey("sitePropertiesId")]
        public virtual SiteProperties siteProperties { get; set; }
    }
}
