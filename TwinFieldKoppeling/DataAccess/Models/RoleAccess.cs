﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class RoleAccess
    {
        public int roleAccessId { get; set; }

        public bool read { get; set; }

        public bool write { get; set; }

        //relationship child to parent
        public int? roleId { get; set; }
        [ForeignKey("roleId")]
        public virtual Role role { get; set; }

        public int? documentId { get; set; }
        [ForeignKey("documentId")]
        public virtual Document document { get; set; }

        public int? documentItemId { get; set; }
        [ForeignKey("documentItemId")]
        public virtual DocumentItem documentItem { get; set; }

        public int? folderId { get; set; }
        [ForeignKey("folderId")]
        public virtual Folder folder { get; set; }

        public int? fileId { get; set; }
        [ForeignKey("fileId")]
        public virtual File file { get; set; }

        //Date
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }
        
    }
}
