﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Address
    {
        //Identifiers
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int addressId { get; set; }

        //Properties
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid addressGuid { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string streetName { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string houseNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string houseNumberSuffix { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string postalCode { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string locality { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string postOfficeBox { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        public int? countryId { get; set; }
        [ForeignKey("countryId")]
        public virtual Country country { get; set; }
    }
}
