﻿using System;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class FileImage : File
    {
        //Identifier
        //[Key]
        //public int fileImageId { get; set; }

        //Properties
        public int width { get; set; }

        public int height { get; set; }

        //Dates
        //public DateTime dateInsert { get; set; }

        //public DateTime dateLastUpdate { get; set; }

        public DateTime dateLastOptimization { get; set; }
    }
}
