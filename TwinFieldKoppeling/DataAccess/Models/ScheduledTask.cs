﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class ScheduledTask
    {
        [Key]
        public int scheduledTaskId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(4000)]
        public string description { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(400)]
        public string url { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(400)]
        public string method { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string scheduleType { get; set; }

        [Column(TypeName = "ntext")]
        public string errorLog { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string recurringTime { get; set; }

        public DateTime? dateOfLastExecution { get; set; }

        public DateTime dateOfNextExecution { get; set; }

        public DateTime dateStarted { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }
        
    }
}
