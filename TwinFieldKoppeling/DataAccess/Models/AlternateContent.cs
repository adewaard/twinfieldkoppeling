﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class AlternateContent
    {
        //Identifier
        [Key]
        public int alternateContentId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string title { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //Relationships from child to parent

        public int? languageId { get; set; }
        [ForeignKey("languageId")]
        public virtual Language language { get; set; }

        public int? documentId { get; set; }
        [ForeignKey("documentId")]
        public virtual Document parentDocument { get; set; }

        public int? linkedDocumentId { get; set; }
        [ForeignKey("linkedDocumentId")]
        public virtual Document linkedDocument { get; set; }
    }
}
