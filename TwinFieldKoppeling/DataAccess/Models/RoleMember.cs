﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class RoleMember
    {
        [Key]
        public int roleMemberId { get; set; }

        public int roleId { get; set; }
        [ForeignKey("roleId")]
        public virtual Role role { get; set; }

        public string applicationUserId { get; set; }
        [ForeignKey("applicationUserId")]
        public virtual ApplicationUser applicationUser { get; set; }
    }
}
