﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class GlobalSetting
    {
        [Key]
        public int globalSettingId { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string moduleFolder { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string iisName { get; set; }

        public int? userFolderId { get; set; }
        [ForeignKey("userFolderId")]
        public virtual Folder userFolder { get; set; }

        public int userImageWidth { get; set; }

        public int userImageHeight { get; set; }

        [Required]
        public bool urlRewriteResources { get; set; }

        [Required]
        public bool disableMsThemeCompatible { get; set; }

        [Required]
        public bool disableImageToolbar { get; set; }

        [Required]
        public bool minifyHtml { get; set; }

        [Required]
        public bool removeEtag { get; set; }

        public int cacheControlMaxAge { get; set; }

        public int afasSubscriptionId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(4000)]
        public string blockedReferers { get; set; }

        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }
    }
}
