﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavGroup
    {
        [Key]
        public int eavGroupId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string name { get; set; }

        public int orderIndex { get; set; }

        public int? eavSetId { get; set; }
        [ForeignKey("eavSetId")]
        public virtual EavSet eavSet { get; set; }

        public virtual ICollection<EavAttribute> eavAttributes { get; set; } 

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        
    }
}
