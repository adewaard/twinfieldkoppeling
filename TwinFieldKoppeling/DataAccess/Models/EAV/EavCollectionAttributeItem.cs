﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavCollectionAttributeItem
    {
        [Key]
        public int eavCollectionAttributeItemId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string name { get; set; }

        public int? order { get; set; }

        public int? eavCollectionAttributeId { get; set; }
        [ForeignKey("eavCollectionAttributeId")]
        public virtual EavCollectionAttribute eavCollectionAttribute { get; set; }

        public virtual ICollection<EavCollectionAttributeValue> eavCollectionAttributeValues { get; set; } 

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
       
    }
}
