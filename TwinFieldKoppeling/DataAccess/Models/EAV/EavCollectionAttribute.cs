﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavCollectionAttribute:EavAttribute
    {
        [Required]
        public bool multiple { get; set; }

        public virtual ICollection<EavCollectionAttributeItem> eavCollectionAttributeItems { get; set; }


    }
}
