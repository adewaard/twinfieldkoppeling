﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavAttributeValue
    {
        [Key]
        public int eavAttributeValueId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(512)]
        [Required(AllowEmptyStrings = true)]
        public string value { get; set; }

        public int? eavAttributeId { get; set; }
        [ForeignKey("eavAttributeId")]
        public virtual EavAttribute eavAttribute { get; set; }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; } 

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        
    }
}
