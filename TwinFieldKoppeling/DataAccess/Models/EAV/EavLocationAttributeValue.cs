﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavLocationAttributeValue : EavAttributeValue
    {
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string coords { get; set; }
    }
}
