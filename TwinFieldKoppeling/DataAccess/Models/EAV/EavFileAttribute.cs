﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavFileAttribute : EavAttribute
    {
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string mimeAccept { get; set; }


    }
}
