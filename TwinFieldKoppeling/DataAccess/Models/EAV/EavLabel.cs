﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavLabel
    {
        [Key]
        public int eavLabelId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string label { get; set; }

        public int? eavAttributeId { get; set; }
        [ForeignKey("eavAttributeId")]
        public virtual EavAttribute eavAttribute { get; set; }

        public int? eavCollectionAttributeItemId { get; set; }
        [ForeignKey("eavCollectionAttributeItemId")]
        public virtual EavCollectionAttributeItem eavCollectionAttributeItem { get; set; }

        public int? storeViewId { get; set; }
        [ForeignKey("storeViewId")]
        public virtual StoreView storeView { get; set; }
        
        //Add CollectionItem

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

       
    }
}
