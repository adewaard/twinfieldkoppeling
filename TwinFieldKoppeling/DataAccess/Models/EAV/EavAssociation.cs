﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.Catalog;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavAssociation
    {
        [Key]
        public int eavAssociationId { get; set; }

        public int? eavAttributevalueId { get; set; }
        [ForeignKey("eavAttributevalueId")]
        public virtual EavAttributeValue eavAttributeValue { get; set; }

        public int? organizationId { get; set; }
        [ForeignKey("organizationId")]
        public virtual Organization organization { get; set; }      

        public int? personId { get; set; }
        [ForeignKey("personId")]
        public virtual Person person { get; set; }

        public int? contactPersonId { get; set; }
        [ForeignKey("contactPersonId")]
        public virtual ContactPerson contactPerson { get; set; }

        #region workflows
        public int? workflowTriggerTypeId { get; set; }
        [ForeignKey("workflowTriggerTypeId")]
        public virtual WorkflowTriggerType workflowTriggerType { get; set; }

        public int? workflowId { get; set; }
        [ForeignKey("workflowId")]
        public virtual Workflow workflow { get; set; }

        public int? templateWorkflowId { get; set; }
        [ForeignKey("templateWorkflowId")]
        public virtual TemplateWorkflow templateWorkflow { get; set; }

        public int? statementId { get; set; }
        [ForeignKey("statementId")]
        public virtual Statement statement { get; set; }

        public int? templateStatementId { get; set; }
        [ForeignKey("templateStatementId")]
        public virtual TemplateStatement templateStatement { get; set; }
        #endregion workflows

        //Catalog
        public int? categoryId { get; set; }
        [ForeignKey("categoryId")]
        public virtual Category category { get; set; }

        public int? productId { get; set; }
        [ForeignKey("productId")]
        public virtual Product product { get; set; }

        //TODO Add more Extensible Modules

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
    }
}
