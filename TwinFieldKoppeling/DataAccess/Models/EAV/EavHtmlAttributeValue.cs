﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavHtmlAttributeValue : EavAttributeValue
    {
        public int? htmlId { get; set; }
        [ForeignKey("htmlId")]
        public virtual Html html { get; set; }
    }
}
