﻿using System.ComponentModel.DataAnnotations;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavBooleanAttributeValue : EavAttributeValue
    {
        [Required]
        public bool isChecked { get; set; }
    }
}
