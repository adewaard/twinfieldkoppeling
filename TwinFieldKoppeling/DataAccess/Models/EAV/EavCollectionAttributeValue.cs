﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavCollectionAttributeValue :EavAttributeValue
    {
        public int? eavCollectionAttributeItemId { get; set; }
        [ForeignKey("eavCollectionAttributeItemId")]
        public virtual EavCollectionAttributeItem eavCollectionAttributeItem { get; set; }
    }
}
