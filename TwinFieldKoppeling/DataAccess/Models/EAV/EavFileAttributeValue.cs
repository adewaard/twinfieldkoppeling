﻿using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavFileAttributeValue : EavAttributeValue
    {
        public int? fileId { get; set; }
        [ForeignKey("fileId")]
        public virtual File file { get; set; }
    }
}
