﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavSet
    {
        [Key]
        public int eavSetId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string name { get; set; }

        public int? moduleId { get; set; }
        [ForeignKey("moduleId")]
        public virtual Module module { get; set; }

        public virtual ICollection<EavGroup> eavGroups { get; set; } 

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

       
    }
}