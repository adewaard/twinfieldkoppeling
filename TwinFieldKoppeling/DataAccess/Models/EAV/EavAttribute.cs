﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.EAV
{
    public class EavAttribute
    {
        [Key]
        public int eavAttributeId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string label { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string type { get; set; }

        public int orderIndex { get; set; }

        public int? moduleId { get; set; }

        [ForeignKey("moduleId")]
        public virtual Module module { get; set; }

        public int? eavSetId { get; set; }

        [ForeignKey("eavSetId")]
        public virtual EavSet eavSet { get; set; }

        public int? eavGroupId { get; set; }

        [ForeignKey("eavGroupId")]
        public virtual EavGroup eavGroup { get; set; }

        public int? iconFileId { get; set; }

        [ForeignKey("iconFileId")]
        public virtual File icon { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<EavLabel> eavLabels { get; set; }

        public virtual ICollection<EavAttributeValue> eavAttributeValues { get; set; }
    }
}
