﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows;

namespace TwinFieldKoppeling.DataAccess.Models.Conversion
{
    public class Conversion
    {
        [Key]
        public int conversionId { get; set; }

        public int conversionStatus { get; set; }

        public decimal? conversionTotalPrice { get; set; }

        public DateTime? conversionDate { get; set; }

        public string @type { get; set; }

        public string conversionReference { get; set; }

        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<Workflow> workflows { get; set; }

        //Relation from parent to child
        public int? organizationId { get; set; }
        [ForeignKey("organizationId")]
        public virtual Organization organization { get; set; }

        //Methods
        public Conversion()
        {
            workflows = new HashSet<Workflow>();
        }
    }
}
