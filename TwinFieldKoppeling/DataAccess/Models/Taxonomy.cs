﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Taxonomy
    {
        [Key]
        public int taxonomyId { get; set; }

        public int? documentId { get; set; }
        [ForeignKey("documentId")]
        public virtual Document document { get; set; }

        public int? documentItemId { get; set; }
        [ForeignKey("documentItemId")]
        public virtual DocumentItem documentItem { get; set; }

        public int? classificationId { get; set; }
        [ForeignKey("classificationId")]
        public virtual Classification classification { get; set; }

        public int? fileId { get; set; }
        [ForeignKey("fileId")]
        public virtual File file { get; set; }

        //TODO FAQ + NEWSITEM + SHOWCASE

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
    }
}
