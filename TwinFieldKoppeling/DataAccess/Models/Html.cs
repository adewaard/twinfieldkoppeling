﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Html
    {
        //Identifier
        [Key]
        public int htmlId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "ntext")]
        public string content { get; set; }

        [Required]
        [Column(TypeName = "ntext")]
        public string parsed { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }
    }
}
