﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class MediaType
    {
        [Key]
        public int mediaTypeId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string name { get; set; }

        //Dates
        [Required]
        public DateTime dateInsert { get; set; }

        [Required]
        public DateTime dateLastUpdate { get; set; }
    }
}
