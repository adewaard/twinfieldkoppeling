﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class MetaItem
    {
        //Identifier
        [Key]
        public int metaItemid { get; set; }

        //Properties
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string title { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string subject { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string date { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string type { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string contributor { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string coverage { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string creator { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string format { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string identifier { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string language { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string publisher { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string relation { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string rights { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string source { get; set; }

        //Dates
        //[ScaffoldColumn(false)]
        public DateTime dateInsert { get; set; }

        //[ScaffoldColumn(false)]
        public DateTime dateLastUpdate { get; set; }        
    }
}
