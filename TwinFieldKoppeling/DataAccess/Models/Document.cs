﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Document
    {
        //Identifiers
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int documentId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid documentGuid { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string name { get; set; }

        public string description { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string Uri { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string order { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string externalUrl { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string sortOrder { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string accessKey { get; set; }

        [Required]
        public bool active { get; set; }

        [Required]
        public bool show { get; set; }

        [Required]
        public bool start { get; set; }

        [Required]
        public bool allowSearchEngines { get; set; }

        public string path { get; set; }

        //Dates
        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime activeFrom { get; set; }

        [Required]
        [DataType(DataType.Date)]
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:dd/MM/yyyy}")]
        public DateTime activeUntil { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime dateInsert { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime dateLastUpdate { get; set; }

        //Relation from child to parent
        public int? parentDocumentId { get; set; }
        [ForeignKey("parentDocumentId")]
        //[InverseProperty("Document")]
        public virtual Document parentDocument { get; set; }

        public int? linkedDocumentId { get; set; }
        [ForeignKey("linkedDocumentId")]
        //[InverseProperty("Document")]
        public virtual Document linkedDocument { get; set; }

        public int? languageId { get; set; }
        [ForeignKey("languageId")]
        public virtual Language language { get; set; }

        public int? templateId { get; set; }
        [ForeignKey("templateId")]
        public virtual Template template { get; set; }

        public int? metaItemid { get; set; }
        [ForeignKey("metaItemid")]
        public virtual MetaItem metaItem { get; set; }

        public int? siteId { get; set; }
        [ForeignKey("siteId")]
        public virtual Site site { get; set; }

        public int? canonicalDocumentId { get; set; }
        [ForeignKey("canonicalDocumentId")]
        public virtual Document canonicalDocument { get; set; }

        //TODO User FK 

        //Relation from parent to child
        [InverseProperty("parentDocument")]
        public virtual ICollection<Document> childDocuments { get; set; }

        [InverseProperty("linkedDocument")]
        public virtual ICollection<Document> linkedDocuments { get; set; }

        public virtual ICollection<DocumentItem> documentItems { get; set; } 

        public virtual ICollection<Uri> uris { get; set; }

        public virtual ICollection<SeoLinkWord> seoLinkWords { get; set; }

        public virtual ICollection<AlternateContent> alternateContents { get; set; } 

        public virtual ICollection<PublishedItem> publishedItems { get; set; }

        public virtual ICollection<Taxonomy> taxonomies { get; set; } 

        public virtual ICollection<RoleAccess> documentRoleAccess { get; set; }

       
    }
}
