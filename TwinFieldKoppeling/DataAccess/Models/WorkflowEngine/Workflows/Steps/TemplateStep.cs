﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps
{
    public class TemplateStep
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int templateStepId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }
        
        public Int64? timeToTriggerTheReminder { get; set; }
        [NotMapped]
        public TimeSpan? TimeSpanValue
        {
            get
            {
                if (timeToTriggerTheReminder.HasValue)
                {
                    return TimeSpan.FromTicks(timeToTriggerTheReminder.Value);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value.HasValue)
                {
                    timeToTriggerTheReminder = value.Value.Ticks;
                }
                else
                {
                    timeToTriggerTheReminder = null;
                }
            }
        }
        
        public int? reminderTemplateStepId { get; set; }
        [ForeignKey("reminderTemplateStepId")]
        public virtual TemplateStep reminderTemplateStep { get; set; }
        [InverseProperty("reminderTemplateStep")]
        public virtual ICollection<TemplateStep> remindeeTemplateSteps { get; set; }
        
        public int? nextTemplateStepId { get; set; }
        [ForeignKey("nextTemplateStepId")]
        public virtual TemplateStep nextTemplateStep { get; set; }
        [InverseProperty("nextTemplateStep")]
        public virtual ICollection<TemplateStep> previousTemplateSteps { get; set; }

        public virtual ICollection<TemplateStatement> templateStatements { get; set; }

        public virtual ICollection<TemplateWorkflow> templateWorkflows { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        

       
    }
}
