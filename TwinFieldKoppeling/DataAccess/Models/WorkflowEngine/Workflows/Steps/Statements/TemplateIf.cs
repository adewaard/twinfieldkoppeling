﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements
{
    public class TemplateIf : TemplateStatement
    {
        public TemplateIf()
        {
            type = "if statement";
        }
        
        public int expressionId { get; set; }

        [ForeignKey("expressionId")] 
        public virtual Expression expression { get; set; }
        
        public virtual ICollection<TemplateStatement> ifTemplateStatements { get; set; }

        public virtual ICollection<TemplateStatement> elseTemplateStatements { get; set; }
    }
}
