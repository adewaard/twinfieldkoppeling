﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements
{
    public class If : Statement
    {
        public If()
        {
            type = "If condition";
        }

        [Required]
        public int expressionId { get; set; }

        [ForeignKey("expressionId")] 
        public virtual Expression expression { get; set; }
        
        public virtual ICollection<Statement> ifStatements { get; set; }

        public virtual ICollection<Statement> elseStatements { get; set; }
        
    }
}
