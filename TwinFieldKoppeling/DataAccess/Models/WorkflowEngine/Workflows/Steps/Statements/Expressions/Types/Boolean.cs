﻿namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types
{
    public class TypeBoolean : Type
    {
        public TypeBoolean()
        {
            name = "Boolean";
        }
        
        public override object getDefaultValue()
        {
            return false;
        }
    }
}
