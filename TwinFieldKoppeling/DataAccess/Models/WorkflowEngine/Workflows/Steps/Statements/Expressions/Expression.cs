﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Values;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions
{
    public abstract class Expression
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int expressionId { get; set; }
        
        public int typeId { get; set; }
        [ForeignKey("typeId")]
        public Type type { get; set; }

        public virtual Value value { get; set; }

        public string expressionType { get; protected set; }

        public System.DateTime dateInsert { get; set; }
        public System.DateTime dateLastUpdate { get; set; }
    }
}
