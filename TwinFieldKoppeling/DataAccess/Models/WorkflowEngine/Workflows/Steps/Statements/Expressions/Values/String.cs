﻿using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Values
{
    public class String : Value
    {
        public string value { get; set; }

        public String(string value)
        {
            this.value = value;
            typeId = TypeRegistrator.getTypeString().typeId;
        }

        public String()
        {
            typeId = TypeRegistrator.getTypeString().typeId;
        }

        public override string ToString()
        {
            return value;
        }

        public override object getValue()
        {
            return value;
        }
    }
}

