﻿using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Values
{
    public class Decimal : Value
    {
        public decimal value { get; set; }

        public Decimal(decimal value)
        {
            this.value = value;
            typeId = TypeRegistrator.getTypeDecimal().typeId;
        }

        public Decimal()
        {
            typeId = TypeRegistrator.getTypeDecimal().typeId;
        }

        public override object getValue()
        {
            return value;
        }
    }
}
