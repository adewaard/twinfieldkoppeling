﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions
{
    public abstract class Unary : Expression
    {
        public int? leftExpressionId { get; set; }

        [ForeignKey("leftExpressionId")]
        public Expression left { get; set; }
    }

    public class Parameter : Unary
    {
        public Parameter()
        {
            expressionType = "Parameter";
        }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        [Required]
        public string name { get; set; }
    }

    public class Constant : Unary
    {
        public Constant()
        {
            expressionType = "Constant";
        }
    }
}
