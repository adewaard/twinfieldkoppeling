﻿namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types
{
    public class TypeDateTime : Type
    {
        public TypeDateTime()
        {
            name = "DateTime";
        }

        public override object getDefaultValue()
        {
            return new System.DateTime();
        }
    }
}
