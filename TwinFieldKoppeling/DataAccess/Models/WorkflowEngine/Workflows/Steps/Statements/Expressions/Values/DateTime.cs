﻿using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Values
{
    public class DateTime : Value
    {
        public System.DateTime value { get; set; }

        public DateTime(System.DateTime value)
        {
            this.value = value;
            typeId = TypeRegistrator.getTypeDateTime().typeId;
        }

        public DateTime()
        {
            typeId = TypeRegistrator.getTypeDateTime().typeId;
        }

        public override object getValue()
        {
            return value;
        }
    }
}
