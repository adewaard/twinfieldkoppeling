﻿namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types
{
    public static class TypeRegistrator
    {
        private static TypeBoolean _typeBoolean;
        private static TypeString _typeString;
        private static TypeDecimal _typeDecimal;
        private static TypeDateTime _typeDateTime;


        public static TypeBoolean getTypeBoolean()
        {
            
            return _typeBoolean;
        }
        
        public static TypeString getTypeString()
        {
            
            return _typeString;
        }
        
        public static TypeDecimal getTypeDecimal()
        {
            
            return _typeDecimal;
        }
        
        public static TypeDateTime getTypeDateTime()
        {
            
            return _typeDateTime;
        }
    }
}
