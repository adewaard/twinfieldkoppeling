﻿using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Values
{
    public class Boolean : Value
    {
        public bool value { get; set; } 

        public Boolean(bool value)
        {
            this.value = value;
            typeId = TypeRegistrator.getTypeBoolean().typeId;
        }

        public Boolean()
        {
            typeId = TypeRegistrator.getTypeBoolean().typeId;
        }

        public override object getValue()
        {
            return value;
        }
    }
}
