﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Values
{
    public abstract class Value
    {
        [Key, ForeignKey("expression")]
        public int valueId { get; set; }

        public int typeId { get; set; }

        [ForeignKey("typeId")]
        public Type type { get; set; }

        public abstract object getValue();
        
        public virtual Expression expression { get; set; }

        public System.DateTime dateInsert { get; set; }
        public System.DateTime dateLastUpdate { get; set; }

        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            Value p = (Value)obj;
            return getValue().Equals(p.getValue());
        }

        public override int GetHashCode()
        {
            return getValue().GetHashCode();
        }
    }
}
