﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Values;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types
{ 
    public abstract class Type
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int typeId { get; set; }
        
        [Index(IsUnique = true)]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string name { get; set; }

        public virtual ICollection<Expression> expressions { get; set; }
        public virtual ICollection<Value> values { get; set; }

        public System.DateTime dateInsert { get; set; }
        public System.DateTime dateLastUpdate { get; set; }

        public abstract object getDefaultValue();

        public override bool Equals(object obj)
        {
            // Check for null values and compare run-time types.
            if (obj == null || GetType() != obj.GetType())
                return false;

            Type o = (Type)obj;
            return name == o.name;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode() ^ name.GetHashCode();
        }
    }
}
