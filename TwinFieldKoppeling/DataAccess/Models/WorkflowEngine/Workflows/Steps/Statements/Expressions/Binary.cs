﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions
{
    public abstract class Binary : Expression
    {
        [Required]
        public int leftExpressionId { get; set; }

        [ForeignKey("leftExpressionId")]
        public Expression left { get; set; }

        [Required]
        public int rightExpressionId { get; set; }

        [ForeignKey("rightExpressionId")]
        public Expression right { get; set; }
    }

    public class GT : Binary
    {
        public GT()
        {
            expressionType = "Greater than";
            typeId = TypeRegistrator.getTypeBoolean().typeId;
        }
    }

    public class GTE : Binary
    {
        public GTE()
        {
            expressionType = "Greater than or equal to";
            typeId = TypeRegistrator.getTypeBoolean().typeId;
        }
    }

    public class LT : Binary
    {
        public LT()
        {
            expressionType = "Less than";
            typeId = TypeRegistrator.getTypeBoolean().typeId;
        }
    }

    public class LTE : Binary
    {
        public LTE()
        {
            expressionType = "Less than or equal to";
            typeId = TypeRegistrator.getTypeBoolean().typeId;
        }
    }

    public class EqualTo : Binary
    {
        public EqualTo()
        {
            expressionType = "Equal to";
            typeId = TypeRegistrator.getTypeBoolean().typeId;
        }
    }

    public class NotEqualTo : Binary
    {
        public NotEqualTo()
        {
            expressionType = "Not equal to";
            typeId = TypeRegistrator.getTypeBoolean().typeId;
        }
    }
}
