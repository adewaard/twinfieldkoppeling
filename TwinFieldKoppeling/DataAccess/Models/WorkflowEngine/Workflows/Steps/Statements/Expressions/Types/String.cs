﻿namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types
{
    public class TypeString : Type
    {
        public TypeString()
        {
            name = "String";
        }

        public override object getDefaultValue()
        {
            return "";
        }
    }
}
