﻿namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements.Expressions.Types
{
    public class TypeDecimal : Type
    {
        public TypeDecimal()
        {
            name = "Decimal";
        }

        public override object getDefaultValue()
        {
            return 0;
        }
    }
}