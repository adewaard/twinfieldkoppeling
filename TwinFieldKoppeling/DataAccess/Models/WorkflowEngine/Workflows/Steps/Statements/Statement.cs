﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements
{
    public abstract class Statement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int statementId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        [Required]
        public string type { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        [Required]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        [Required]
        public int executionStatus { get; set; }

        public int stepId { get; set; }
        [ForeignKey("stepId")]
        public virtual Step step { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        public int? ifParentStatementInIfBlockId { get; set; }

        [ForeignKey("ifParentStatementInIfBlockId")]
        public virtual If ifParentStatementInIfBlock { get; set; }

        public int? ifParentStatementInElseBlockId { get; set; }

        [ForeignKey("ifParentStatementInElseBlockId")]
        public virtual If ifParentStatementInElseBlock { get; set; }

        public Statement getParentStatement()
        {
            return ifParentStatementInIfBlock ?? ifParentStatementInElseBlock;
        }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; }
    }
}
