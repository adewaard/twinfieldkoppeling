﻿using System.Collections.Generic;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements
{
    public class Action : Statement
    {
        public Action()
        {
            type = "action";
            eavAssociations = new List<EavAssociation>();
        }
    }
}
