﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements
{
    public abstract class TemplateStatement
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int templateStatementId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        [Required]
        public string type { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        [Required]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        public int? ifParentTemplateStatementInIfBlockId { get; set; }

        [ForeignKey("ifParentTemplateStatementInIfBlockId")]
        public virtual TemplateIf ifParentTemplateStatementInIfBlock { get; set; }

        public int? ifParentTemplateStatementInElseBlockId { get; set; }

        [ForeignKey("ifParentTemplateStatementInElseBlockId")]
        public virtual TemplateIf ifParentTemplateStatementInElseBlock { get; set; }

        public TemplateStatement getParentStatement()
        {
            return ifParentTemplateStatementInIfBlock ?? ifParentTemplateStatementInElseBlock;
        }

        public virtual ICollection<TemplateStep> templateSteps { get; set; }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; }
    }
}
