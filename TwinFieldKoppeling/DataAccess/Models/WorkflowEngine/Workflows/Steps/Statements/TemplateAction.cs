﻿using System;
using System.Collections.Generic;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements
{
    public class TemplateAction : TemplateStatement
    {
        public TemplateAction()
        {
            type = "templateAction";
            eavAssociations = new List<EavAssociation>();
        }
        
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
    }
}
