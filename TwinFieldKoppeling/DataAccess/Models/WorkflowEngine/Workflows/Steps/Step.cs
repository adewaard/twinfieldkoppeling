﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps.Statements;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps
{
    public class Step
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int stepId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        public bool completed { get; set; }

        public int? reminderStepId { get; set; }
        [ForeignKey("reminderStepId")]
        public virtual Step reminderStep { get; set; }
        [InverseProperty("reminderStep")]
        public virtual ICollection<Step> remindeeSteps { get; set; }
        
        public int? nextStepId { get; set; }
        [ForeignKey("nextStepId")]
        public virtual Step nextStep { get; set; }
        [InverseProperty("nextStep")]
        public virtual ICollection<Step> previousSteps { get; set; }

        public Int64? timeToTriggerTheReminder { get; set; }
        [NotMapped]
        public TimeSpan? TimeSpanValue
        {
            get
            {
                if (timeToTriggerTheReminder.HasValue)
                {
                    return TimeSpan.FromTicks(timeToTriggerTheReminder.Value);
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value.HasValue)
                {
                    timeToTriggerTheReminder = value.Value.Ticks;
                }
                else
                {
                    timeToTriggerTheReminder = null;
                }
            }
        }

        [NotMapped]
        public DateTime TimeToTriggerTheReminder
        {
            get { return dateInsert.Add(TimeSpanValue.GetValueOrDefault()); }
        }

        public virtual ICollection<Statement> statements { get; set; }
         
        public int workflowId { get; set; }
        [ForeignKey("workflowId")]
        public virtual Workflow workflow { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
    }
}
