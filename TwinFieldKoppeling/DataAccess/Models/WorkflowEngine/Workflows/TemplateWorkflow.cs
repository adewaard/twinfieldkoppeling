﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows
{
    public class TemplateWorkflow
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int templateWorkflowId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        public int workflowTriggerTypeId { get; set; }
        [ForeignKey("workflowTriggerTypeId")]
        public virtual WorkflowTriggerType workflowTriggerType { get; set; }
        
        public int initialTemplateStepId { get; set; }
        [ForeignKey("initialTemplateStepId")]
        public virtual TemplateStep initialTemplateStep { get; set; }

        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<Workflow> workflows { get; set; }
    }
}
