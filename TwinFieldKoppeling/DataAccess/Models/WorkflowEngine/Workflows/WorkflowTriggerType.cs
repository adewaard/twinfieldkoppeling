﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows
{
    public class WorkflowTriggerType
    {
        [Key]
        public int workflowTriggerTypeId { get; set; }

        public string eventName { get; set; }

        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; }
    }
}
