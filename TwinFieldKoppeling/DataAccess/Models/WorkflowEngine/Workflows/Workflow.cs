﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.EAV;
using TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows.Steps;

namespace TwinFieldKoppeling.DataAccess.Models.WorkflowEngine.Workflows
{
    public class Workflow
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int workflowId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid workflowGuid { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        public bool completed { get; set; }

        public int templateWorkflowId { get; set; }
        [ForeignKey("templateWorkflowId")]
        public virtual TemplateWorkflow templateWorkflow { get; set; }

        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<Step> stepsExecuted { get; set; }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; }

        public int? conversionId { get; set; }
        [ForeignKey("conversionId")]
        public virtual Conversion.Conversion conversion { get; set; }

        public Workflow()
        {
            eavAssociations = new HashSet<EavAssociation>();
            stepsExecuted = new HashSet<Step>();
        }
    }
}
