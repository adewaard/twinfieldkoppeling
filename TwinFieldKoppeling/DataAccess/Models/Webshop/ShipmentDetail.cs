﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Webshop
{
    public class ShipmentDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int shipmentDetailId { get; set; }

        //Properties
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid shipmentDetailGuid { get; set; }

        [Required]
        public int quantity { get; set; }

        public int? orderDetailId { get; set; }
        [ForeignKey("orderDetailId")]
        public virtual OrderDetail orderDetail { get; set; }

        public int? shipmentId { get; set; }
        [ForeignKey("shipmentId")]
        public virtual Shipment shipment { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
    }
}
