﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.Catalog;

namespace TwinFieldKoppeling.DataAccess.Models.Webshop
{
    public class OrderDetail
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int orderDetailId { get; set; }

        //Properties
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid orderDetailGuid { get; set; }

        [Required]
        public int quantity { get; set; }

        [Required]
        [Column(TypeName = "Money")]
        public decimal price { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        [Required]
        public string reference { get; set; }

        public int? productId { get; set; }
        [ForeignKey("productId")]
        public virtual Product product { get; set; }

        public virtual ICollection<ShipmentDetail> shipmentDetails { get; set; } 

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        public int? conversionId { get; set; }
        [ForeignKey("conversionId")]
        public virtual Order order { get; set; }

        public int? relatedOrderDetailId { get; set; }
        [ForeignKey("relatedOrderDetailId")]
        public virtual OrderDetail relatedOrderDetail { get; set; }
    }
}
