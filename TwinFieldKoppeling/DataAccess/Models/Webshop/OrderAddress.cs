﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Webshop
{
    public class OrderAddress : Address
    {
        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        [Required]
        public string firstName { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string lastName { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string companyName { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string emailAddress { get; set; }
    }
}
