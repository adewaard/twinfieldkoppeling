﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Webshop
{
    public class Order// : Conversion.Conversion

{
    [Key]
    //[DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int orderId { get; set; }

    //Properties
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public Guid orderGuid { get; set; }

    [Column(TypeName = "nvarchar")]
    [MaxLength(24)]
    [Required]
    public string orderNumber { get; set; }

    [Required]
    public int orderStatus { get; set; }

    [Required]
    public int paymentStatus { get; set; }

    public decimal discount { get; set; }

    public decimal totalPrice { get; set; }

    public decimal shipping { get; set; }

    [Column(TypeName = "nvarchar")]
    [MaxLength(24)]
    public string ipAddress { get; set; }

    [Column(TypeName = "nvarchar")]
    [MaxLength(50)]
    public string voucherCode { get; set; }

    [Column(TypeName = "nvarchar")]
    [MaxLength(50)]
    public string reference { get; set; }

    [Column(TypeName = "nvarchar")]
    [MaxLength(50)]
    public string commentsCustomer { get; set; }

    [Column(TypeName = "nvarchar")]
    [MaxLength(50)]
    public string commentsWebshop { get; set; }

    //Dates
    //public DateTime dateInsert { get; set; }
    //public DateTime dateLastUpdate { get; set; }
    public DateTime dateLastUpdateByMsp { get; set; }

    //public string applicationUserId { get; set; }

    //[ForeignKey("applicationUserId")]
    //public virtual ApplicationUser user { get; set; }

    public int? languageId { get; set; }

    [ForeignKey("languageId")]
    public virtual Language language { get; set; }

    public int? siteId { get; set; }

    [ForeignKey("siteId")]
    public virtual Site site { get; set; }

    public int? deliveryAddressId { get; set; }

    [ForeignKey("deliveryAddressId")]
    public virtual OrderAddress deliveryAddress { get; set; }

    public int? invoiceAddressId { get; set; }

    [ForeignKey("invoiceAddressId")]
    public virtual OrderAddress invoiceAddress { get; set; }

    public int? organizationId { get; set; }

    [ForeignKey("organizationId")]
    public virtual Organization organization { get; set; }

    public int? personId { get; set; }

    [ForeignKey("personId")]
    public virtual Person person { get; set; }

    public int? contactPersonId { get; set; }

    [ForeignKey("contactPersonId")]
    public virtual ContactPerson contactPerson { get; set; }

    public virtual ICollection<OrderDetail> orderDetails { get; set; }

    public virtual ICollection<OrderOption> orderOptions { get; set; }

    public DateTime? dateDelivery { get; set; }

    public int STATUS_ORDER_CREATED, STATUS_ORDER_PROCESSED, STATUS_ORDER_SEND, STATUS_ORDER_CANCELLED;

    public Order()
    {
        STATUS_ORDER_CREATED = 1;
        STATUS_ORDER_PROCESSED = 2;
        STATUS_ORDER_SEND = 3;
        STATUS_ORDER_CANCELLED = 9;
    }


}
}
