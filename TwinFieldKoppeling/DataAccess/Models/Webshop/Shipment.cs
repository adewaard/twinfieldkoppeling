﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Webshop
{
    public class Shipment
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int shipmentId { get; set; }

        //Properties
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid shipmentGuid { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(24)]
        [Required]
        public string invoiceNumber { get; set; }

        public int shipmentStatus { get; set; }

        public virtual ICollection<ShipmentDetail> shipmentDetails { get; set; }

        public int? linkedShipmentId { get; set; }
        [ForeignKey("linkedShipmentId")]
        public virtual Shipment linkedShipment { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
       
    }
}
