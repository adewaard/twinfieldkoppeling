﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Webshop
{
    public class WebshopSetting
    {
        [Key]
        public int webshopSettingsId { get; set; }

        public int? accountDocumentId { get; set; }
        [ForeignKey("accountDocumentId")]
        public virtual Document accountDocument { get; set; }

        public int? registerDocumentId { get; set; }
        [ForeignKey("registerDocumentId")]
        public virtual Document registerDocument { get; set; }

        public int? loginDocumentId { get; set; }
        [ForeignKey("loginDocumentId")]
        public virtual Document loginDocument { get; set; }

        public int? shoppingCartDocumentId { get; set; }
        [ForeignKey("shoppingCartDocumentId")]
        public virtual Document shoppingCartDocument { get; set; }

        public int? languageId { get; set; }
        [ForeignKey("languageId")]
        public virtual Language language { get; set; }

        public int? siteId { get; set; }
        [ForeignKey("siteId")]
        public virtual Site site { get; set; }

        [Required]
        public bool includeVatInPrices { get; set; }

        public int? completeMessageHtmlId { get; set; }
        [ForeignKey("completeMessageHtmlId")]
        public virtual Html completeMessage { get; set; }

        public string applicationUserId { get; set; }
        [ForeignKey("applicationUserId")]
        public virtual ApplicationUser loggedOutUser { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
        
    }
}
