﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text.RegularExpressions;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Person
    {
        //Identifiers
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int personId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid personGuid { get; set; }

        //Properties
        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string internalNumber { get; set; }

        public int? mailingListStatus { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(20)]
        public string initials { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string firstname { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string lastname { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(20)]
        public string preposition { get; set; }

        public int gender { get; set; }//follows ISO 5218 rules: http://en.wikipedia.org/wiki/ISO_5218 

        public DateTime dateOfBirth { get; set; }

        public int? addressId { get; set; }
        [ForeignKey("addressId")]
        public virtual Address address { get; set; }

        public int? postalAddressId { get; set; }
        [ForeignKey("postalAddressId")]
        public virtual Address postalAddress { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string emailAddress { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string phoneNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string mobilePhoneNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string faxNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string linkedIn { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string twitter { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string facebook { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string googlePlus { get; set; }

        public bool active { get; set; }

        public bool optIn { get; set; }

        //Relation from parent to chuild
        //Add reseller

        public string applicationUserId { get; set; }
        [ForeignKey("applicationUserId")]
        public virtual ApplicationUser user { get; set; }

        public int? fileId { get; set; }
        [ForeignKey("fileId")]
        public virtual File file { get; set; }

        public virtual ICollection<ContactPerson> contactPersons { get; set; } 

        public virtual ICollection<EavAssociation> eavAssociations { get; set; } 

        //Add eavProperties

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        //Return fullname of person
        public string getFullName()
        {
            string fullName = firstname + " " + preposition + " " + lastname;
            fullName = Regex.Replace(fullName, @"\s+", " ", RegexOptions.IgnoreCase);
            return fullName;
        }
        
    }
}
