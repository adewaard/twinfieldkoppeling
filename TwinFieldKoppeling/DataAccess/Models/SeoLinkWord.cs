﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class SeoLinkWord
    {
        //Identifier
        [Key]
        public int seoLinkWordId { get; set; }

        //Properties
        [MaxLength(255)]
        [Column(TypeName = "nvarchar")]
        public string name { get; set; }

        //Relations from child to parent
        public int? documentId { get; set; }
        [ForeignKey("documentId")]
        public virtual Document document { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

    }
}
