﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.EAV;
using TwinFieldKoppeling.DataAccess.Models.Webshop;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Organization
    {
        //Identifiers
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int organizationId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid organizationGuid { get; set; }

        //Properties
        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string internalNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string vatNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string registrationNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string emailAddress { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string phoneNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string mobilePhoneNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string faxNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string linkedIn { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string twitter { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string facebook { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string googlePlus { get; set; }

        public bool active { get; set; }

        //Relation from parent to child
        public int? addressId { get; set; }
        [ForeignKey("addressId")]
        public virtual Address address { get; set; }

        public int? postalAddressId { get; set; }
        [ForeignKey("postalAddressId")]
        public virtual Address postalAddress { get; set; }

        public virtual ICollection<ContactPerson> contactPersons { get; set; }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; } 

        public virtual ICollection<Order> orders { get; set; }  

        public virtual ICollection<Conversion.Conversion>  conversions { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

       
    }

}
