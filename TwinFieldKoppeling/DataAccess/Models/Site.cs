﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Site
    {
        //identifier
        [Key]
        public int siteId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string title { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string emailaddress { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string domain { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string redirectDomain { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string googleAnalyticsKey { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string bingKey { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string yandexKey { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string baseUri { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string titleSeparator { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string breadCrumbsSeparator { get; set; }

        [Required]
        public bool multipleLanguages { get; set; }

        [Required]
        public bool noWwwEnabled { get; set; }

        [Required]
        public bool sslEnabled { get; set; }

        [Required]
        public bool startDocumentHasNoUri { get; set; }

        public bool? localRecipientsEnabled { get; set; }

        public bool? shouldUseCdn { get; set; }


        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //Relations from child to parent
        public int? folderId { get; set; }
        [ForeignKey("folderId")]
        public virtual Folder folder { get; set; }

        public int? templateId { get; set; }
        [ForeignKey("templateId")]
        public virtual Template template { get; set; }

        public int? languageId { get; set; }
        [ForeignKey("languageId")]
        public virtual Language language { get; set; }

        public int? awsSettingsId { get; set; }
        [ForeignKey("awsSettingsId")]
        public virtual AwsSetting awsSetting { get; set; }

        public virtual ICollection<SiteProperties> siteProperties { get; set; }

        //Relationships from parent to child
        public virtual ICollection<Css> css { get; set; }
        
        public virtual ICollection<Javascript> javascript { get; set; }   

        public virtual ICollection<Folder> folders { get; set; }

        public virtual ICollection<Document> documents { get; set; }

        public virtual ICollection<DocumentItem> documentItems { get; set; }

        //public virtual ICollection<Template> templates { get; set; }

        public virtual ICollection<Language> multipleSiteLanguages { get; set; }

        public virtual ICollection<ApplicationUser> users { get; set; }

        public virtual ICollection<StoreView> storeViews { get; set; }
    }
}
