﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Uri
    {
        //Identifier
        [Key]
        public int uriId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(1024)]
        public string uri { get; set; }

        //Date
        public DateTime dateInsert { get; set; }

        //Relations from child to parent
        public int? documentId { get; set; }
        [ForeignKey("documentId")]
        public virtual Document document { get; set; }

        public int? fileId { get; set; }
        [ForeignKey("fileId")]
        public virtual File file { get; set; }
        
    }
}
