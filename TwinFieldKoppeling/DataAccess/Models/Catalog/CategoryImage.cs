﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class CategoryImage
    {
        [Key]
        public int categoryImageId { get; set; }

        public virtual ICollection<CategoryImageFile> categoryImageFiles { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(5)]
        public string order { get; set; }

        [Required]
        public bool primary { get; set; }

        public int? categoryId { get; set; }
        [ForeignKey("categoryId")]
        public virtual Category category { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
    }
}
