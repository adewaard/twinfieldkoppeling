﻿using System.Collections.Generic;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class ConfigurableProduct : Product
    {
        public virtual ICollection<AssociatedProduct> associatedProducts { get; set; }
        
        //Add EAV Collection Products 
    }
}
