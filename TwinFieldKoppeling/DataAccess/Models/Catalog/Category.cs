﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class Category
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int categoryId { get; set; }

        //Properties
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid categoryGuid { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string title { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string uri { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        [Required]
        public string order { get; set; }

        [Column(TypeName = "ntext")]
        public string shortDescription { get; set; }

        public int? htmlId { get; set; }
        [ForeignKey("htmlId")]
        public virtual Html longDescription { get; set; }

        public int? templateId { get; set; }
        [ForeignKey("templateId")]
        public virtual Template template { get; set; }

        public int? metaItemId { get; set; }
        [ForeignKey("metaItemId")]
        public virtual MetaItem metaItem { get; set; }

        public int? storeViewId { get; set; }
        [ForeignKey("storeViewId")]
        public virtual StoreView storeView { get; set; }

        public int? parentId { get; set; }
        [ForeignKey("parentId")]
        public virtual Category parent { get; set; }

        public int? canonicalCategoryId { get; set; }
        [ForeignKey("canonicalCategoryId")]
        public virtual Category canonical { get; set; }

        public int? listImageId { get; set; }
        [ForeignKey("listImageId")]
        public virtual FileImage listImage { get; set; }

        public int? largeImageId { get; set; }
        [ForeignKey("largeImageId")]
        public virtual FileImage largeImage { get; set; }

        [InverseProperty("parent")]
        public virtual ICollection<Category> children { get; set; }

        [Required]
        public bool active { get; set; }

        [Required]
        public bool show { get; set; }

        [Required]
        public bool publishProducts { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<CategoryImage> categoryImages { get; set; } 

        public virtual ICollection<Product> products { get; set; }

        public virtual ICollection<FeaturedProduct> featuredProducts { get; set; }

        //EAV
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string eavSetName { get; set; }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; }

        //TODO PUBLISHEDITEMS + DISPLAY EAV PROPERTIES
    }
}
