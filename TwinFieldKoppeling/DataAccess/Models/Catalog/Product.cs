﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class Product
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int productId { get; set; }

        //Properties
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid productGuid { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string title { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string sku { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string uri { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        [Required]
        public string order { get; set; }

        public decimal oldPrice { get; set; }

        public decimal price { get; set; }

        public int? vatRateId { get; set; }
        [ForeignKey("vatRateId")]
        public virtual VatRate vatRate { get; set; }

        public int stock { get; set; }

        [Column(TypeName = "ntext")]
        public string shortDescription { get; set; }

        public int? htmlId { get; set; }
        [ForeignKey("htmlId")]
        public virtual Html longDescription { get; set; }

        public int? templateId { get; set; }
        [ForeignKey("templateId")]
        public virtual Template template { get; set; }

        public int? metaItemId { get; set; }
        [ForeignKey("metaItemId")]
        public virtual MetaItem metaItem { get; set; }

        [Required]
        public bool active { get; set; }

        [Required]
        public bool show { get; set; }

        [Required]
        public bool backOrderEnabled { get; set; }

        [Required]
        public bool layAwayEnabled { get; set; }

        [Required]
        public bool deleted { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<Category> categories { get; set; }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; }
    }
}
