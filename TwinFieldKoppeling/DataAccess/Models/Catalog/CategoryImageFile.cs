﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class CategoryImageFile : FileImage
    {
        [Column(TypeName = "nvarchar")]
        [MaxLength(5)]
        public string order { get; set; }

        public int? categoryImageId { get; set; }
        [ForeignKey("categoryImageId")]
        public virtual CategoryImage categoryImage { get; set; }

        public int? imageTypeId { get; set; }
        [ForeignKey("imageTypeId")]
        public virtual ImageType imageType { get; set; }
    }
}
