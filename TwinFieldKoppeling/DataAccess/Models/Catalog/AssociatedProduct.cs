﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class AssociatedProduct
    {
        [Key]
        public int associatedProductId { get; set; }

        public int? configurableProductId { get; set; }
        [ForeignKey("configurableProductId")]
        public virtual ConfigurableProduct configurableProduct { get; set; }

        public int? productId { get; set; }
        [ForeignKey("productId")]
        public virtual Product product { get; set; }
    }
}
