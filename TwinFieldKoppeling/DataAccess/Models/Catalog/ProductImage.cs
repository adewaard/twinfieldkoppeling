﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class ProductImage
    {
        [Key]
        public int productImageId { get; set; }

        public virtual ICollection<ProductImageFile> productImageFiles { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(5)]
        public string order { get; set; }

        [Required]
        public bool primary { get; set; }

        public int? productId { get; set; }
        [ForeignKey("productId")]
        public virtual Product product { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
    }
}
