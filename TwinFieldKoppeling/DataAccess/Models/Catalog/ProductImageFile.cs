﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class ProductImageFile : FileImage
    {
        [Column(TypeName = "nvarchar")]
        [MaxLength(5)]
        public string order { get; set; }

        public int? productImageId { get; set; }
        [ForeignKey("productImageId")]
        public virtual ProductImage productImage { get; set; }

        public int? imageTypeId { get; set; }
        [ForeignKey("imageTypeId")]
        public virtual ImageType imageType { get; set; }
    }
}
