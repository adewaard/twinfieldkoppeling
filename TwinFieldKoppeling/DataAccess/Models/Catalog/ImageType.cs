﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models.Catalog
{
    public class ImageType
    {
        [Key]
        public int imageTypeId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string title { get; set; }

        public int width { get; set; }
        public int height { get; set; }
        public int type { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(5)]
        public string order { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }
    }
}
