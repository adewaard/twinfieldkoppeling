﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Language
    {
        //Identifier
        [Key]
        public int languageId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string language { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string code { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string isoCode { get; set; }

        [Required]
        public bool active { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //relationship child to parent
        public int? fileImageId { get; set; }
        [ForeignKey("fileImageId")]
        public virtual FileImage fileImage { get; set; }

        //relationship parent to child
        public virtual ICollection<Document> documents { get; set; }

        public virtual ICollection<AlternateContent> alternateContents { get; set; }

        public virtual ICollection<Site> multipleSitesWithLanguages { get; set; }


        public virtual ICollection<StoreView> storeViews { get; set; }


    }
}
