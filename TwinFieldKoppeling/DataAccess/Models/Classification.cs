﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Classification
    {
        [Key]
        public int classificationId { get; set; }

        [Required]
        public string name { get; set; }

        public int? moduleId { get; set; }
        [ForeignKey("moduleId")]
        public virtual Module module { get; set; }

        public int? parentClassificationId { get; set; }
        [ForeignKey("parentClassificationId")]
        public virtual Classification parentClassification { get; set; }

        [InverseProperty("parentClassification")]
        public virtual ICollection<Classification> childClassifications { get; set; }

        public virtual ICollection<File> files { get; set; }

        public virtual ICollection<Document> documents { get; set; }

        public virtual ICollection<DocumentItem> documentItems { get; set; }

        [Column(TypeName = "datetime2")]
        public DateTime dateInsert { get; set; }
        [Column(TypeName = "datetime2")]
        public DateTime dateLastUpdate { get; set; }


    }
}
