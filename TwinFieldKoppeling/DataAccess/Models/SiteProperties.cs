﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class SiteProperties
    {
        //Identifier
        [Key]
        //[DatabaseGeneratedAttribute(DatabaseGeneratedOption.Identity)]
        public int sitePropertiesId { get; set;}

        //Properties
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string companyName { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string emailAddress { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string telephoneNumber { get; set; }

                [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string vatNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string companyRegistrationNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string bankAccountNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string bank { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string facebookUrl { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string googlePlusUrl { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string youtubeUrl { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string pinterestUrl { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string linkedInUrl { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string twitterUrl { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //relations from child to parent
        public int? visitAddressId { get; set; }
        [ForeignKey("visitAddressId")]
        public virtual Address visitAddress { get; set; }

        public int? deliveryAddressId { get; set; }
        [ForeignKey("deliveryAddressId")]
        public virtual Address deliveryAddress { get; set; }

        public int? siteId { get; set; }
        [ForeignKey("siteId")]
        public virtual Site site { get; set; }

        public int? faviconId { get; set; }
        [ForeignKey("faviconId")]
        public virtual File favicon { get; set; }

        //touchIcons
        public virtual ICollection<TouchIcon> touchIcons { get; set; } 

    }
}
