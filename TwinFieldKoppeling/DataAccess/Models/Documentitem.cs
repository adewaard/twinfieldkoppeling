﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class DocumentItem
    {
        //Identifiers
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int documentItemId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid documentItemGuid { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string name { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string order { get; set; }

        public bool isComponent { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string component { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string method { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string methodArguments { get; set; }

        public bool active { get; set; }

        public int areaCode { get; set; }

        //Dates
        public DateTime activeFrom { get; set; }

        public DateTime activeUntil { get; set; }

        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //Relations from child to parent
        public int? siteId { get; set; }
        [ForeignKey("siteId")]
        public virtual Site site { get; set; }

        public int? linkedDocumentItemId { get; set; }
        [ForeignKey("linkedDocumentItemId")]
        public virtual DocumentItem linkedDocumentItem { get; set; }

        [InverseProperty("linkedDocumentItem")]
        public virtual ICollection<DocumentItem> linkedDocumentItems { get; set; } 

        public int? documentId { get; set; }
        [ForeignKey("documentId")]
        public virtual Document document { get; set; }

        public int? languageId { get; set; }
        [ForeignKey("languageId")]
        public virtual Language language { get; set; }

        public int? htmlId { get; set; }
        [ForeignKey("htmlId")]
        public virtual Html html { get; set; }

        public virtual ICollection<Taxonomy> taxonomies { get; set; }

        public virtual ICollection<RoleAccess> documentItemRoleAccess { get; set; }

        //TODO Classifications + documentItemRoleAccess
    }
}
