﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class PublishedItem
    {
        //Identifier
        [Key]
        public int publishedItemId { get; set; }

        //Properties
        [Required]
        public bool isPrimary { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //Relation from child to parent
        public int? documentId { get; set; }
        [ForeignKey("documentId")]
        public virtual Document document { get; set; }
    }
}
