﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Module
    {
        [Key]
        public int moduleId { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string name { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string nameEn { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string nameDotNet { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string version { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string viewName { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string descriptionEn { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string order { get; set; }

        [Required]
        public bool @public {get;set;}

        [Required]
        public bool isExtensible { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        // relation from child to parent
        public int? parentModuleId { get; set; }
        [ForeignKey("parentModuleId")]
        public virtual Module parentModule { get; set; }

        // relation from parent to child
        [InverseProperty("parentModule")]
        public virtual ICollection<Module> modules { get; set; }

        public virtual ICollection<Css> css { get; set; }

        public virtual ICollection<Javascript> javascript { get; set; }

        public virtual ICollection<Classification> classifications { get; set; } 

        public virtual ICollection<EavSet> eavSets { get; set; } 


    }
}
