﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Template
    {
        //Identifier
        [Key]
        public int templateId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string name { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        [Required]
        [Column(TypeName = "ntext")]
        public string text { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(50)]
        public string moduleName { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string areaCodes { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //Relation from child to parent
        public int? siteId { get; set; }
        [ForeignKey("siteId")]
        public virtual Site site { get; set; }

        public int? moduleId { get; set; }
        [ForeignKey("moduleId")]
        public virtual Module module { get; set; }

        public virtual ICollection<Site> sites { get; set; }
        
    }
}
