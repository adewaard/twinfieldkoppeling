﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Folder
    {
        //Identifier
        [Key]
        public int folderId { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string name { get; set; }

        public int? nrOfChildren { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

        //Relationships
        public int? parentFolderId { get; set; }
        [ForeignKey("parentFolderId")]
        public virtual Folder parentFolder { get; set; }

        [InverseProperty("parentFolder")]
        public virtual ICollection<Folder> childFolders { get; set; } 

        public int? siteId { get; set; }
        [ForeignKey("siteId")]
        public virtual Site site { get; set; }

        public virtual ICollection<File> files { get; set; }

        public virtual ICollection<RoleAccess> folderRoleAccess { get; set; }

        

    }
}
