﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.Catalog;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class StoreView
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int storeViewId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid storeViewGuid { get; set; }

        //Properties
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        [Required]
        public string name { get; set; }

        [Column(TypeName = "ntext")]
        public string description { get; set; }

        public int? languageId { get; set; }
        [ForeignKey("languageId")]
        public virtual Language language { get; set; }

        public int? siteId { get; set; }
        [ForeignKey("siteId")]
        public virtual Site site { get; set; }

        [Required]
        public bool @default { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<Category> categories { get; set; } 
        
    }
}
