﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TwinFieldKoppeling.DataAccess.Models.EAV;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class ContactPerson
    {
        //Identifiers
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int contactPersonId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid contactPersonGuid { get; set; }

        //Properties
        [Column(TypeName = "nvarchar")]
        [MaxLength(100)]
        public string internalNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string department { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string position { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string emailAddress { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string phoneNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string mobilePhoneNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string faxNumber { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string linkedIn { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string twitter { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string facebook { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string googlePlus { get; set; }

        public bool active { get; set; }

        public bool optIn { get; set; }

        public int? mailingListStatus { get; set; }

        public string languageCode { get; set; }

        public string formName { get; set; }

        //Relation from parent to child
        public int? addressId { get; set; }
        [ForeignKey("addressId")]
        public virtual Address address { get; set; }

        public int? postalAddressId { get; set; }
        [ForeignKey("postalAddressId")]
        public virtual Address postalAddress { get; set; }

        public int? personId { get; set; }
        [ForeignKey("personId")]
        public virtual Person person { get; set; }

        public int? organizationId { get; set; }
        [ForeignKey("organizationId")]
        public virtual Organization organization { get; set; }

        public string applicationUserId { get; set; }
        [ForeignKey("applicationUserId")]
        public virtual ApplicationUser user { get; set; }

        public DateTime dateInsert { get; set; }
        public DateTime dateLastUpdate { get; set; }

        public virtual ICollection<EavAssociation> eavAssociations { get; set; } 
       
    }
}
