﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class File
    {
        //Identifiers
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int fileId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid fileGuid { get; set; }

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string name { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string description { get; set; }

        [Required]
        [Column(TypeName = "float")]
        public double fileSize { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string contentType { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string path { get; set; }

        //Dates
        public DateTime dateInsert { get; set;}

        public DateTime dateLastUpdate { get; set; }

        //Relationships from child to parent
        public int? sourceFileId { get; set; }
        [ForeignKey("sourceFileId")]
        public virtual File sourceFile { get; set; }

        [InverseProperty("sourceFile")]
        public virtual ICollection<File> sourceFiles { get; set; }

        public int? folderId { get; set; }
        [ForeignKey("folderId")]
        public virtual Folder folder { get; set; }

        //Relationships from parent to child
        public virtual ICollection<Uri> uris { get; set; }

        public virtual ICollection<RoleAccess> fileRoleAccess { get; set; }
        //TODO fileRoleAccess + classifications

        
    }
}
