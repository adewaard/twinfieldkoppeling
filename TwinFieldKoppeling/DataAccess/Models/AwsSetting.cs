﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class AwsSetting
    {
        [Key]
        public int awsSettingsId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string origin { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string domainName { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string staticDomain { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string cNames { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string distributionId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string accessId { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string secretKey { get; set; }

        [Column(TypeName = "nvarchar")]
        [MaxLength(255)]
        public string comment { get; set; }
        
        public DateTime dateInsert { get; set; }
        
        public DateTime dateLastUpdate { get; set; }
    }
}
