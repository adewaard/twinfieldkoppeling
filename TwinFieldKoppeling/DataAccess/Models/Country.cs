﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace TwinFieldKoppeling.DataAccess.Models
{
    public class Country
    {
        //Identifier
        [Key]
        public int countryId { get; set;}

        //Properties
        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(150)]
        public string country { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string code { get; set; }

        [Required]
        [Column(TypeName = "nvarchar")]
        [MaxLength(10)]
        public string isoCode { get; set; }

        //Dates
        public DateTime dateInsert { get; set; }

        public DateTime dateLastUpdate { get; set; }

    }
}
