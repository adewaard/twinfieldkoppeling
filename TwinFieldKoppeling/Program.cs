﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Xml;
using System.Xml.Linq;
using log4net;
using TwinFieldKoppeling.com.twinfield.c3;
using TwinFieldKoppeling.com.twinfield.login;
using TwinFieldKoppeling.DataAccess;
using TwinFieldKoppeling.DataAccess.Models.Catalog;
using TwinFieldKoppeling.Models;
using Header = TwinFieldKoppeling.com.twinfield.c3.Header;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Configuration.Install;

namespace TwinFieldKoppeling
{
    public class Program : ServiceBase
    {
        private static readonly ILog Logger = LogManager.GetLogger(typeof(Program));
        private Timer timer;

        //static void Main(string[] args)
        //{
        //    Console.Title = typeof(Program).Name;
        //    log4net.Config.XmlConfigurator.Configure();
        //    Logger.Info("Started TwinfieldKoppeling Service");

        //    Run();
        //}

        private static void Main(string[] args)
        {
            //if (!Environment.UserInteractive)
            //{
            //    ServiceBase.Run(new Program());
            //}
            //else
            //{
            //    new Program();
            //}

            if (Environment.UserInteractive)
            {
                new Program();
                new Helper().deleteLogs();
                Console.WriteLine("Press any key to stop program");
                Console.Read();
                
            }
            else
            {
                ServiceBase.Run(new Program());
            }
        }

        public Program()
        {
            Logger.Info("Starting TwinfieldKoppeling Service...");
            this.ServiceName = "TwinfieldKoppeling Service";

            //InitializeComponent();
            timer = new Timer(10000);
            timer.AutoReset = true;
            timer.Elapsed += new ElapsedEventHandler(timer_elasped);

            TwinfieldLogin();
            
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                timer.Start();     
                TwinfieldLogin();          
            }
            catch (Exception ex)
            {
                //log anywhere
            }
        }

        private void timer_elasped(object sender, ElapsedEventArgs e)
        {
            try
            {
                TwinfieldLogin();
                new Helper().deleteLogs();
            }
            catch (Exception ex)
            {
                Logger.Error("Error Message: " + ex.Message.ToString(), ex);
            }
        }

        protected override void OnStop()
        {
            if (timer != null)
            {
                timer.Stop();
            }
        }


        public static void Run()
        {
            while (true)
            {
                var consoleInput = ReadFromConsole();
                if (string.IsNullOrWhiteSpace(consoleInput)) continue;

                try
                {
                    // Execute the command:
                    if (consoleInput == "login")
                    {
                        TwinfieldLogin();
                    }

                    if (consoleInput == "test")
                    {
                        test();
                    }
                }
                catch (Exception ex)
                {
                    // OOPS! Something went wrong - Write out the problem:
                    WriteToConsole(ex.Message);
                }
            }
        }

        public static void test()
        {
            log4net.Config.XmlConfigurator.Configure();
            log4net.ILog log = log4net.LogManager.GetLogger(typeof(Program));
            try
            {
                string str = String.Empty;
                string subStr = str.Substring(0, 4); //this line will create error as the string "str" is empty.  
            }
            catch (Exception ex)
            {
                log.Error("Error Message: " + ex.Message.ToString(), ex);
            }


        }


        static string Execute(string command)
        {
            // We'll make this more interesting shortly:
            return string.Format("Executed the {0} Command", command);
        }


        public static void WriteToConsole(string message = "")
        {
            if (message.Length > 0)
            {
                Console.WriteLine(message);
            }
        }


        const string _readPrompt = "console> ";
        public static string ReadFromConsole(string promptMessage = "")
        {
            // Show a prompt, and get input:
            Console.Write(_readPrompt + promptMessage);
            return Console.ReadLine();
        }

        public static void TwinfieldLogin()
        {
            try
            {

                Session session = new Session();

                // Request the user, password and organisation. 
                //Console.WriteLine("User name:");
                string user = ConfigurationManager.AppSettings["twinfieldUsername"].Trim();
                //Console.WriteLine("Password:");
                string password = ConfigurationManager.AppSettings["twinfieldPassword"].Trim();
                //Console.WriteLine("Organisation:");
                string organisation = ConfigurationManager.AppSettings["twinfieldOrganization"].Trim();

                // Log on to the session. 
                LogonAction nextAction;
                string cluster;
                LogonResult logonResult = session.Logon(user, password, organisation, out nextAction, out cluster);

                if (logonResult == LogonResult.Ok)
                {
                    session.Url = cluster + "/webservices/session.asmx";
                    new Invoice().getConversionsWithStatusFinal(session.HeaderValue.SessionID, cluster);
                }
                else
                {
                    Logger.Error(logonResult.ToString());
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Error logging into Twinfield: " + ex.Message.ToString(), ex);
            }
        }

        

        
    }
}
